﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class responsible to subdivides space and save icosidodecahedron data.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class SpaceDivision {

    private List<int>[] _space;
    private Vector3 _corner;
    private float _sizeCube;
    private Dictionary<Vector3, IcosidodecahedronList> _icosiSaved;
    private int _posNewCenter;

    /// <summary>
    /// Builder
    /// </summary>
    /// <param name="halfSize">The half size of the render distance.</param>
    /// <param name="center">Center of the render cube.</param>
    public SpaceDivision(float halfSize, Vector3 center) {
        _space = new List<int>[27];
        _icosiSaved = new Dictionary<Vector3, IcosidodecahedronList>();
        _corner = center - new Vector3(halfSize, halfSize, halfSize);
        _sizeCube = (halfSize * 2) / 3;
        for (int i = 0; i < 27; i++)
            _space[i] = new List<int>();
    }
    
    /// <summary>
    /// Save a icosidodecahedron data
    /// </summary>
    /// <param name="numberIcosi">Number of the icosidodecahedron</param>
    /// <param name="pos">The center position of icosidodecahedron.</param>
    public void Add(int numberIcosi, Vector3 pos) {
        _space[PlaneX(pos.x) + (PlaneZ(pos.z) * 3) + (PlaneY(pos.y) * 9)].Add(numberIcosi);
    }

    /// <summary>
    /// Move the center of the render cube.
    /// </summary>
    /// <param name="center">The new position.</param>
    /// <returns>Return a list with the icosidodecahedron outside of the render cube.</returns>
    public List<int> MoveCenter(Vector3 center) {
        List<int> elemToSave = new List<int>();
        _posNewCenter = PlaneX(center.x) + (PlaneZ(center.z) * 3) + (PlaneY(center.y) * 9);

        switch (_posNewCenter) {
            case 0:
                elemToSave = CleanCubes(0, 2, 0, 2, 0, 2);
                _space[26].AddRange(_space[13]);
                _space[13].Clear();
                _space[13].AddRange(_space[0]);
                _space[14].AddRange(_space[1]);
                _space[16].AddRange(_space[3]);
                _space[17].AddRange(_space[4]);
                _space[22].AddRange(_space[9]);
                _space[23].AddRange(_space[10]);
                _space[25].AddRange(_space[12]);

                _space[0].Clear();
                _space[1].Clear();
                _space[3].Clear();
                _space[4].Clear();
                _space[9].Clear();
                _space[10].Clear();
                _space[12].Clear();
                break;
            case 1:
                elemToSave = CleanCubes(0, 3, 0, 2, 0, 2);
                _space[24].AddRange(_space[12]);
                _space[25].AddRange(_space[13]);
                _space[26].AddRange(_space[14]);
                _space[12].Clear();
                _space[13].Clear();
                _space[14].Clear();

                _space[12].AddRange(_space[0]);
                _space[13].AddRange(_space[1]);
                _space[14].AddRange(_space[2]);
                _space[15].AddRange(_space[3]);
                _space[16].AddRange(_space[4]);
                _space[17].AddRange(_space[5]);
                _space[21].AddRange(_space[9]);
                _space[22].AddRange(_space[10]);
                _space[23].AddRange(_space[11]);
                _space[0].Clear();
                _space[1].Clear();
                _space[2].Clear();
                _space[3].Clear();
                _space[4].Clear();
                _space[5].Clear();
                _space[9].Clear();
                _space[10].Clear();
                _space[11].Clear();
                break;
            case 2:
                elemToSave = CleanCubes(1, 3, 0, 2, 0, 2);
                _space[24].AddRange(_space[13]);
                _space[13].Clear();

                _space[12].AddRange(_space[1]);
                _space[13].AddRange(_space[2]);
                _space[15].AddRange(_space[4]);
                _space[16].AddRange(_space[5]);
                _space[21].AddRange(_space[10]);
                _space[22].AddRange(_space[11]);
                _space[25].AddRange(_space[14]);
                _space[1].Clear();
                _space[2].Clear();
                _space[4].Clear();
                _space[5].Clear();
                _space[10].Clear();
                _space[11].Clear();
                _space[14].Clear();
                break;
            case 3:
                elemToSave = CleanCubes(0, 2, 0, 3, 0, 2);
                _space[20].AddRange(_space[10]);
                _space[23].AddRange(_space[13]);
                _space[26].AddRange(_space[16]);
                _space[10].Clear();
                _space[13].Clear();
                _space[16].Clear();

                _space[10].AddRange(_space[0]);
                _space[11].AddRange(_space[1]);
                _space[13].AddRange(_space[3]);
                _space[14].AddRange(_space[4]);
                _space[16].AddRange(_space[6]);
                _space[17].AddRange(_space[7]);
                _space[19].AddRange(_space[9]);
                _space[22].AddRange(_space[12]);
                _space[25].AddRange(_space[15]);
                _space[0].Clear();
                _space[1].Clear();
                _space[3].Clear();
                _space[4].Clear();
                _space[6].Clear();
                _space[7].Clear();
                _space[9].Clear();
                _space[12].Clear();
                _space[15].Clear();
                break;
            case 4:
                elemToSave = CleanCubes(0, 3, 0, 3, 0, 2);
                _space[18].AddRange(_space[9]);
                _space[19].AddRange(_space[10]);
                _space[20].AddRange(_space[11]);
                _space[21].AddRange(_space[12]);
                _space[22].AddRange(_space[13]);
                _space[23].AddRange(_space[14]);
                _space[24].AddRange(_space[15]);
                _space[25].AddRange(_space[16]);
                _space[26].AddRange(_space[17]);
                _space[9].Clear();
                _space[10].Clear();
                _space[11].Clear();
                _space[12].Clear();
                _space[13].Clear();
                _space[14].Clear();
                _space[15].Clear();
                _space[16].Clear();
                _space[17].Clear();

                _space[9].AddRange(_space[0]);
                _space[10].AddRange(_space[1]);
                _space[11].AddRange(_space[2]);
                _space[12].AddRange(_space[3]);
                _space[13].AddRange(_space[4]);
                _space[14].AddRange(_space[5]);
                _space[15].AddRange(_space[6]);
                _space[16].AddRange(_space[7]);
                _space[17].AddRange(_space[8]);
                _space[0].Clear();
                _space[1].Clear();
                _space[2].Clear();
                _space[3].Clear();
                _space[4].Clear();
                _space[5].Clear();
                _space[6].Clear();
                _space[7].Clear();
                _space[8].Clear();
                break;
            case 5:
                elemToSave = CleanCubes(1, 3, 0, 3, 0, 2);
                _space[18].AddRange(_space[10]);
                _space[21].AddRange(_space[13]);
                _space[24].AddRange(_space[16]);
                _space[10].Clear();
                _space[13].Clear();
                _space[16].Clear();

                _space[9].AddRange(_space[1]);
                _space[10].AddRange(_space[2]);
                _space[12].AddRange(_space[4]);
                _space[13].AddRange(_space[5]);
                _space[15].AddRange(_space[7]);
                _space[16].AddRange(_space[8]);
                _space[19].AddRange(_space[11]);
                _space[22].AddRange(_space[14]);
                _space[25].AddRange(_space[17]);
                _space[1].Clear();
                _space[2].Clear();
                _space[4].Clear();
                _space[5].Clear();
                _space[7].Clear();
                _space[8].Clear();
                _space[11].Clear();
                _space[14].Clear();
                _space[17].Clear();
                break;
            case 6:
                elemToSave = CleanCubes(0, 2, 1, 3, 0, 2);
                _space[20].AddRange(_space[13]);
                _space[13].Clear();

                _space[10].AddRange(_space[3]);
                _space[11].AddRange(_space[4]);
                _space[13].AddRange(_space[6]);
                _space[14].AddRange(_space[7]);
                _space[19].AddRange(_space[12]);
                _space[22].AddRange(_space[15]);
                _space[23].AddRange(_space[16]);
                _space[3].Clear();
                _space[4].Clear();
                _space[6].Clear();
                _space[7].Clear();
                _space[12].Clear();
                _space[15].Clear();
                _space[16].Clear();
                break;
            case 7:
                elemToSave = CleanCubes(0, 3, 1, 3, 0, 2);
                _space[18].AddRange(_space[12]);
                _space[19].AddRange(_space[13]);
                _space[20].AddRange(_space[14]);
                _space[12].Clear();
                _space[13].Clear();
                _space[14].Clear();

                _space[9].AddRange(_space[3]);
                _space[10].AddRange(_space[4]);
                _space[11].AddRange(_space[5]);
                _space[12].AddRange(_space[6]);
                _space[13].AddRange(_space[7]);
                _space[14].AddRange(_space[8]);
                _space[21].AddRange(_space[15]);
                _space[22].AddRange(_space[16]);
                _space[23].AddRange(_space[17]);
                _space[3].Clear();
                _space[4].Clear();
                _space[5].Clear();
                _space[6].Clear();
                _space[7].Clear();
                _space[8].Clear();
                _space[15].Clear();
                _space[16].Clear();
                _space[17].Clear();
                break;
            case 8:
                elemToSave = CleanCubes(1, 3, 1, 3, 0, 2);
                _space[18].AddRange(_space[13]);
                _space[13].Clear();

                _space[9].AddRange(_space[4]);
                _space[10].AddRange(_space[5]);
                _space[12].AddRange(_space[7]);
                _space[13].AddRange(_space[8]);
                _space[19].AddRange(_space[14]);
                _space[21].AddRange(_space[16]);
                _space[22].AddRange(_space[17]);
                _space[4].Clear();
                _space[5].Clear();
                _space[7].Clear();
                _space[8].Clear();
                _space[14].Clear();
                _space[16].Clear();
                _space[17].Clear();
                break;
            case 9:
                elemToSave = CleanCubes(0, 2, 0, 2, 0, 3);
                _space[8].AddRange(_space[4]);
                _space[17].AddRange(_space[13]);
                _space[26].AddRange(_space[22]);
                _space[4].Clear();
                _space[13].Clear();
                _space[22].Clear();

                _space[4].AddRange(_space[0]);
                _space[5].AddRange(_space[1]);
                _space[7].AddRange(_space[3]);
                _space[13].AddRange(_space[9]);
                _space[14].AddRange(_space[10]);
                _space[16].AddRange(_space[12]);
                _space[22].AddRange(_space[18]);
                _space[23].AddRange(_space[19]);
                _space[25].AddRange(_space[21]);
                _space[0].Clear();
                _space[1].Clear();
                _space[3].Clear();
                _space[9].Clear();
                _space[10].Clear();
                _space[12].Clear();
                _space[18].Clear();
                _space[19].Clear();
                _space[21].Clear();
                break;
            case 10:
                elemToSave = CleanCubes(0, 3, 0, 2, 0, 3);

                _space[6].AddRange(_space[3]);
                _space[7].AddRange(_space[4]);
                _space[8].AddRange(_space[5]);
                _space[15].AddRange(_space[12]);
                _space[16].AddRange(_space[13]);
                _space[17].AddRange(_space[14]);
                _space[24].AddRange(_space[21]);
                _space[25].AddRange(_space[22]);
                _space[26].AddRange(_space[23]);
                _space[3].Clear();
                _space[4].Clear();
                _space[5].Clear();
                _space[12].Clear();
                _space[13].Clear();
                _space[14].Clear();
                _space[21].Clear();
                _space[22].Clear();
                _space[23].Clear();

                _space[3].AddRange(_space[0]);
                _space[4].AddRange(_space[1]);
                _space[5].AddRange(_space[2]);
                _space[12].AddRange(_space[9]);
                _space[13].AddRange(_space[10]);
                _space[14].AddRange(_space[11]);
                _space[21].AddRange(_space[18]);
                _space[22].AddRange(_space[19]);
                _space[23].AddRange(_space[20]);
                _space[0].Clear();
                _space[1].Clear();
                _space[2].Clear();
                _space[9].Clear();
                _space[10].Clear();
                _space[11].Clear();
                _space[18].Clear();
                _space[19].Clear();
                _space[20].Clear();
                break;
            case 11:
                elemToSave = CleanCubes(1, 3, 0, 2, 0, 3);
                _space[6].AddRange(_space[4]);
                _space[15].AddRange(_space[13]);
                _space[24].AddRange(_space[22]);
                _space[4].Clear();
                _space[13].Clear();
                _space[22].Clear();

                _space[3].AddRange(_space[1]);
                _space[4].AddRange(_space[2]);
                _space[7].AddRange(_space[5]);
                _space[12].AddRange(_space[10]);
                _space[13].AddRange(_space[11]);
                _space[16].AddRange(_space[14]);
                _space[21].AddRange(_space[19]);
                _space[22].AddRange(_space[20]);
                _space[25].AddRange(_space[23]);
                _space[1].Clear();
                _space[2].Clear();
                _space[5].Clear();
                _space[10].Clear();
                _space[11].Clear();
                _space[14].Clear();
                _space[19].Clear();
                _space[20].Clear();
                _space[23].Clear();
                break;
            case 12:
                elemToSave = CleanCubes(0, 2, 0, 3, 0, 3);
                _space[2].AddRange(_space[1]);
                _space[4].AddRange(_space[3]);
                _space[8].AddRange(_space[7]);
                _space[11].AddRange(_space[10]);
                _space[14].AddRange(_space[13]);
                _space[17].AddRange(_space[16]);
                _space[20].AddRange(_space[19]);
                _space[23].AddRange(_space[22]);
                _space[26].AddRange(_space[25]);
                _space[1].Clear();
                _space[3].Clear();
                _space[7].Clear();
                _space[10].Clear();
                _space[13].Clear();
                _space[16].Clear();
                _space[19].Clear();
                _space[22].Clear();
                _space[25].Clear();

                _space[1].AddRange(_space[0]);
                _space[5].AddRange(_space[4]);
                _space[7].AddRange(_space[6]);
                _space[10].AddRange(_space[9]);
                _space[13].AddRange(_space[12]);
                _space[16].AddRange(_space[15]);
                _space[19].AddRange(_space[18]);
                _space[22].AddRange(_space[21]);
                _space[25].AddRange(_space[24]);
                _space[0].Clear();
                _space[4].Clear();
                _space[6].Clear();
                _space[9].Clear();
                _space[12].Clear();
                _space[15].Clear();
                _space[18].Clear();
                _space[21].Clear();
                _space[24].Clear();
                break;
            case 13:
                return elemToSave;
            case 14:
                elemToSave = CleanCubes(1, 3, 0, 3, 0, 3);
                _space[0].AddRange(_space[1]);
                _space[3].AddRange(_space[4]);
                _space[6].AddRange(_space[7]);
                _space[9].AddRange(_space[10]);
                _space[12].AddRange(_space[13]);
                _space[15].AddRange(_space[16]);
                _space[18].AddRange(_space[19]);
                _space[21].AddRange(_space[22]);
                _space[24].AddRange(_space[25]);
                _space[1].Clear();
                _space[4].Clear();
                _space[7].Clear();
                _space[10].Clear();
                _space[13].Clear();
                _space[16].Clear();
                _space[19].Clear();
                _space[22].Clear();
                _space[25].Clear();

                _space[1].AddRange(_space[2]);
                _space[4].AddRange(_space[5]);
                _space[7].AddRange(_space[8]);
                _space[10].AddRange(_space[11]);
                _space[13].AddRange(_space[14]);
                _space[16].AddRange(_space[17]);
                _space[19].AddRange(_space[20]);
                _space[22].AddRange(_space[23]);
                _space[25].AddRange(_space[26]);
                _space[2].Clear();
                _space[5].Clear();
                _space[8].Clear();
                _space[11].Clear();
                _space[14].Clear();
                _space[17].Clear();
                _space[20].Clear();
                _space[23].Clear();
                _space[26].Clear();
                break;
            case 15:
                elemToSave = CleanCubes(0, 2, 1, 3, 0, 3);
                _space[2].AddRange(_space[4]);
                _space[11].AddRange(_space[13]);
                _space[20].AddRange(_space[22]);
                _space[4].Clear();
                _space[13].Clear();
                _space[22].Clear();

                _space[1].AddRange(_space[3]);
                _space[4].AddRange(_space[6]);
                _space[5].AddRange(_space[7]);
                _space[10].AddRange(_space[12]);
                _space[13].AddRange(_space[15]);
                _space[14].AddRange(_space[16]);
                _space[19].AddRange(_space[21]);
                _space[22].AddRange(_space[24]);
                _space[23].AddRange(_space[25]);
                _space[3].Clear();
                _space[6].Clear();
                _space[7].Clear();
                _space[12].Clear();
                _space[15].Clear();
                _space[16].Clear();
                _space[21].Clear();
                _space[24].Clear();
                _space[25].Clear();
                break;
            case 16:
                elemToSave = CleanCubes(0, 3, 1, 3, 0, 3);
                _space[0].AddRange(_space[3]);
                _space[1].AddRange(_space[4]);
                _space[2].AddRange(_space[5]);
                _space[9].AddRange(_space[12]);
                _space[10].AddRange(_space[13]);
                _space[11].AddRange(_space[14]);
                _space[18].AddRange(_space[21]);
                _space[19].AddRange(_space[22]);
                _space[20].AddRange(_space[23]);
                _space[3].Clear();
                _space[4].Clear();
                _space[5].Clear();
                _space[12].Clear();
                _space[13].Clear();
                _space[14].Clear();
                _space[21].Clear();
                _space[22].Clear();
                _space[23].Clear();

                _space[3].AddRange(_space[6]);
                _space[4].AddRange(_space[7]);
                _space[5].AddRange(_space[8]);
                _space[12].AddRange(_space[15]);
                _space[13].AddRange(_space[16]);
                _space[14].AddRange(_space[17]);
                _space[21].AddRange(_space[24]);
                _space[22].AddRange(_space[25]);
                _space[23].AddRange(_space[26]);
                _space[6].Clear();
                _space[7].Clear();
                _space[8].Clear();
                _space[15].Clear();
                _space[16].Clear();
                _space[17].Clear();
                _space[24].Clear();
                _space[25].Clear();
                _space[26].Clear();
                break;
            case 17:
                elemToSave = CleanCubes(1, 3, 1, 3, 0, 3);
                _space[0].AddRange(_space[4]);
                _space[9].AddRange(_space[13]);
                _space[18].AddRange(_space[22]);
                _space[4].Clear();
                _space[13].Clear();
                _space[22].Clear();

                _space[1].AddRange(_space[5]);
                _space[3].AddRange(_space[7]);
                _space[4].AddRange(_space[8]);
                _space[10].AddRange(_space[14]);
                _space[12].AddRange(_space[16]);
                _space[13].AddRange(_space[17]);
                _space[19].AddRange(_space[23]);
                _space[21].AddRange(_space[25]);
                _space[22].AddRange(_space[26]);
                _space[5].Clear();
                _space[7].Clear();
                _space[8].Clear();
                _space[14].Clear();
                _space[16].Clear();
                _space[17].Clear();
                _space[23].Clear();
                _space[25].Clear();
                _space[26].Clear();
                break;
            case 18:
                elemToSave = CleanCubes(0, 2, 0, 2, 1, 3);
                _space[8].AddRange(_space[13]);
                _space[13].Clear();

                _space[4].AddRange(_space[9]);
                _space[5].AddRange(_space[10]);
                _space[7].AddRange(_space[12]);
                _space[13].AddRange(_space[18]);
                _space[14].AddRange(_space[19]);
                _space[16].AddRange(_space[21]);
                _space[17].AddRange(_space[22]);
                _space[9].Clear();
                _space[10].Clear();
                _space[12].Clear();
                _space[18].Clear();
                _space[19].Clear();
                _space[21].Clear();
                _space[22].Clear();
                break;
            case 19:
                elemToSave = CleanCubes(0, 3, 0, 2, 1, 3);
                _space[6].AddRange(_space[12]);
                _space[7].AddRange(_space[13]);
                _space[8].AddRange(_space[14]);
                _space[12].Clear();
                _space[13].Clear();
                _space[14].Clear();

                _space[3].AddRange(_space[9]);
                _space[4].AddRange(_space[10]);
                _space[5].AddRange(_space[11]);
                _space[12].AddRange(_space[18]);
                _space[13].AddRange(_space[19]);
                _space[14].AddRange(_space[20]);
                _space[15].AddRange(_space[21]);
                _space[16].AddRange(_space[22]);
                _space[17].AddRange(_space[23]);
                _space[9].Clear();
                _space[10].Clear();
                _space[11].Clear();
                _space[18].Clear();
                _space[19].Clear();
                _space[20].Clear();
                _space[21].Clear();
                _space[22].Clear();
                _space[23].Clear();
                _space[24].Clear();
                break;
            case 20:
                elemToSave = CleanCubes(1, 3, 0, 2, 1, 3);
                _space[6].AddRange(_space[13]);
                _space[13].Clear();

                _space[3].AddRange(_space[10]);
                _space[4].AddRange(_space[11]);
                _space[7].AddRange(_space[14]);
                _space[12].AddRange(_space[19]);
                _space[13].AddRange(_space[20]);
                _space[15].AddRange(_space[22]);
                _space[16].AddRange(_space[23]);
                _space[10].Clear();
                _space[11].Clear();
                _space[14].Clear();
                _space[19].Clear();
                _space[20].Clear();
                _space[22].Clear();
                _space[23].Clear();
                break;
            case 21:
                elemToSave = CleanCubes(0, 2, 0, 3, 1, 3);
                _space[2].AddRange(_space[10]);
                _space[5].AddRange(_space[13]);
                _space[8].AddRange(_space[16]);
                _space[10].Clear();
                _space[13].Clear();
                _space[16].Clear();

                _space[1].AddRange(_space[9]);
                _space[4].AddRange(_space[12]);
                _space[7].AddRange(_space[15]);
                _space[10].AddRange(_space[18]);
                _space[11].AddRange(_space[19]);
                _space[13].AddRange(_space[21]);
                _space[14].AddRange(_space[22]);
                _space[16].AddRange(_space[24]);
                _space[17].AddRange(_space[25]);
                _space[9].Clear();
                _space[12].Clear();
                _space[15].Clear();
                _space[18].Clear();
                _space[19].Clear();
                _space[21].Clear();
                _space[22].Clear();
                _space[24].Clear();
                _space[25].Clear();
                break;
            case 22:
                elemToSave = CleanCubes(0, 3, 0, 3, 1, 3);
                _space[0].AddRange(_space[9]);
                _space[1].AddRange(_space[10]);
                _space[2].AddRange(_space[11]);
                _space[3].AddRange(_space[12]);
                _space[4].AddRange(_space[13]);
                _space[5].AddRange(_space[14]);
                _space[6].AddRange(_space[15]);
                _space[7].AddRange(_space[16]);
                _space[8].AddRange(_space[17]);
                _space[9].Clear();
                _space[10].Clear();
                _space[11].Clear();
                _space[12].Clear();
                _space[13].Clear();
                _space[14].Clear();
                _space[15].Clear();
                _space[16].Clear();
                _space[17].Clear();

                _space[9].AddRange(_space[18]);
                _space[10].AddRange(_space[19]);
                _space[11].AddRange(_space[20]);
                _space[12].AddRange(_space[21]);
                _space[13].AddRange(_space[22]);
                _space[14].AddRange(_space[23]);
                _space[15].AddRange(_space[24]);
                _space[16].AddRange(_space[25]);
                _space[17].AddRange(_space[26]);
                _space[18].Clear();
                _space[19].Clear();
                _space[20].Clear();
                _space[21].Clear();
                _space[22].Clear();
                _space[23].Clear();
                _space[24].Clear();
                _space[25].Clear();
                _space[26].Clear();
                break;
            case 23:
                elemToSave = CleanCubes(1, 3, 0, 3, 1, 3);
                _space[0].AddRange(_space[10]);
                _space[3].AddRange(_space[13]);
                _space[6].AddRange(_space[16]);
                _space[10].Clear();
                _space[13].Clear();
                _space[16].Clear();

                _space[1].AddRange(_space[11]);
                _space[4].AddRange(_space[14]);
                _space[7].AddRange(_space[17]);
                _space[9].AddRange(_space[19]);
                _space[10].AddRange(_space[20]);
                _space[12].AddRange(_space[22]);
                _space[13].AddRange(_space[23]);
                _space[15].AddRange(_space[25]);
                _space[16].AddRange(_space[26]);
                _space[11].Clear();
                _space[14].Clear();
                _space[17].Clear();
                _space[19].Clear();
                _space[20].Clear();
                _space[22].Clear();
                _space[23].Clear();
                _space[25].Clear();
                _space[26].Clear();
                break;
            case 24:
                elemToSave = CleanCubes(0, 2, 1, 3, 1, 3);
                _space[2].AddRange(_space[13]);
                _space[13].Clear();

                _space[1].AddRange(_space[12]);
                _space[4].AddRange(_space[15]);
                _space[5].AddRange(_space[16]);
                _space[10].AddRange(_space[21]);
                _space[11].AddRange(_space[22]);
                _space[13].AddRange(_space[24]);
                _space[14].AddRange(_space[25]);
                _space[12].Clear();
                _space[15].Clear();
                _space[16].Clear();
                _space[21].Clear();
                _space[22].Clear();
                _space[24].Clear();
                _space[25].Clear();
                break;
            case 25:
                elemToSave = CleanCubes(0, 3, 1, 3, 1, 3);
                _space[0].AddRange(_space[12]);
                _space[1].AddRange(_space[13]);
                _space[2].AddRange(_space[14]);
                _space[12].Clear();
                _space[13].Clear();
                _space[14].Clear();

                _space[3].AddRange(_space[15]);
                _space[4].AddRange(_space[16]);
                _space[5].AddRange(_space[17]);
                _space[9].AddRange(_space[21]);
                _space[10].AddRange(_space[22]);
                _space[11].AddRange(_space[23]);
                _space[12].AddRange(_space[24]);
                _space[13].AddRange(_space[25]);
                _space[14].AddRange(_space[26]);
                _space[15].Clear();
                _space[16].Clear();
                _space[17].Clear();
                _space[21].Clear();
                _space[22].Clear();
                _space[23].Clear();
                _space[24].Clear();
                _space[25].Clear();
                _space[26].Clear();
                break;
            case 26:
                elemToSave = CleanCubes(1, 3, 1, 3, 1, 3);
                _space[0].AddRange(_space[13]);
                _space[13].Clear();

                _space[1].AddRange(_space[14]);
                _space[3].AddRange(_space[16]);
                _space[4].AddRange(_space[17]);
                _space[9].AddRange(_space[22]);
                _space[10].AddRange(_space[23]);
                _space[12].AddRange(_space[25]);
                _space[13].AddRange(_space[26]);
                _space[14].Clear();
                _space[16].Clear();
                _space[17].Clear();
                _space[22].Clear();
                _space[23].Clear();
                _space[25].Clear();
                _space[26].Clear();
                break;
        }
        return elemToSave;
    }

    /// <summary>
    /// Clean the subcubes of the cube render.
    /// </summary>
    /// <param name="xmin">Min position in x to select a subcube.</param>
    /// <param name="xmax">Max position in x to select a subcube.</param>
    /// <param name="zmin">Min position in z to select a subcube.</param>
    /// <param name="zmax">Max position in z to select a subcube.</param>
    /// <param name="ymin">Min position in y to select a subcube.</param>
    /// <param name="ymax">Max position in y to select a subcube.</param>
    /// <returns>Return a list with the icosidodecahedrons as resault of this clean.</returns>
    private List<int> CleanCubes(int xmin, int xmax, int zmin, int zmax, int ymin, int ymax) {
        List<int> cubeToDelete = new List<int>();
        List<int> elemToSave = new List<int>();

        for (int i = 0; i < 27; i++)
            cubeToDelete.Add(i);

        for (int y = ymin; y < ymax; y++)
            for (int z = zmin; z < zmax; z++)
                for (int x = xmin; x < xmax; x++)
                    cubeToDelete.Remove(x + z * 3 + y * 9);

        foreach (int elem in cubeToDelete) {
            elemToSave.AddRange(_space[elem]);
            _space[elem].Clear();
        }

        return elemToSave;
    }

    /// <summary>
    /// Save a list of icosiodecahedron inside a subcube.
    /// </summary>
    /// <param name="toSave">List of icosidodechaedron to storage.</param>
    /// <param name="newcenter">Center of subcube.</param>
    public void SaveIcosi(IcosidodecahedronList toSave, Vector3 newcenter) {
        IcosidodecahedronList[] listToSave = new IcosidodecahedronList[27];
        float halfSize = (_sizeCube * 3) / 2;
        Vector3 oldCenter = _corner + new Vector3(halfSize, halfSize, halfSize);
        int cont;

        for (cont = 0; cont < 27; cont++)
            listToSave[cont] = new IcosidodecahedronList();

        while (!toSave.IsEmpty()) {
            Icosidodecahedron actualIcosi = toSave.GetNext();
            int pos = PlaneX(actualIcosi.GetCenter().x) + (PlaneZ(actualIcosi.GetCenter().z) * 3) + (PlaneY(actualIcosi.GetCenter().y) * 9);
            listToSave[pos].Add(actualIcosi);
        }
        cont = 0;
        for (int y = -1; y < 2; y++) {
            for (int z = -1; z < 2; z++) {
                for (int x = -1; x < 2; x++) {
                    if (listToSave[cont].Count() > 0) {
                        IcosidodecahedronList temp = new IcosidodecahedronList();
                        if (_icosiSaved.TryGetValue((oldCenter + new Vector3( x * _sizeCube, y * _sizeCube, z * _sizeCube)), out temp)) {
                            _icosiSaved.Remove((oldCenter + new Vector3(x * _sizeCube, y * _sizeCube, z * _sizeCube)));
                            listToSave[cont].AddRange(temp);
                        }
                        _icosiSaved.Add((oldCenter + new Vector3(x * _sizeCube, y * _sizeCube, z * _sizeCube)), listToSave[cont]);
                    }
                    cont++;
                }
            }
        }
        _corner = newcenter - new Vector3(halfSize, halfSize, halfSize);
    }

    /// <summary>
    ///  Get the icosidodechaedron removed from de cube render in the change center.
    /// </summary>
    /// <param name="newCenter">Center of subcube.</param>
    /// <returns>Return a list with the icosidodecahedron inside of a subcube.</returns>
    public IcosidodecahedronList GetIcosis(Vector3 newCenter) {

        switch (_posNewCenter) {
            case 26:
                return GetNeighbours(newCenter, -1, 1, -1, 1, -1, 1);
            case 25:
                return GetNeighbours(newCenter, -1, 2, -1, 1, -1, 1);
            case 24:
                return GetNeighbours(newCenter, 0, 2, -1, 1, -1, 1);
            case 23:
                return GetNeighbours(newCenter, -1, 1, -1, 2, -1, 1);
            case 22:
                return GetNeighbours(newCenter, -1, 2, -1, 2, -1, 1);
            case 21:
                return GetNeighbours(newCenter, 0, 2, -1, 2, -1, 1);
            case 20:
                return GetNeighbours(newCenter, -1, 1, 0, 2, -1, 1);
            case 19:
                return GetNeighbours(newCenter, -1, 2, 0, 2, -1, 1);
            case 18:
                return GetNeighbours(newCenter, 0, 2, 0, 2, -1, 1);
            case 17:
                return GetNeighbours(newCenter, -1, 1, -1, 1, -1, 2);
            case 16:
                return GetNeighbours(newCenter, -1, 2, -1, 1, -1, 2);
            case 15:
                return GetNeighbours(newCenter, 0, 2, -1, 1, -1, 2);
            case 14:
                return GetNeighbours(newCenter, -1, 1, -1, 2, -1, 2);
            case 13:
                return new IcosidodecahedronList();
            case 12:
                return GetNeighbours(newCenter, 0, 2, -1, 2, -1, 2);
            case 11:
                return GetNeighbours(newCenter, -1, 1, 0, 2, -1, 2);
            case 10:
                return GetNeighbours(newCenter, -1, 2, 0, 2, -1, 2);
            case 9:
                return GetNeighbours(newCenter, 0, 2, 0, 2, -1, 2);
            case 8:
                return GetNeighbours(newCenter, -1, 1, -1, 1, 0, 2);
            case 7:
                return GetNeighbours(newCenter, -1, 2, -1, 1, 0, 2);
            case 6:
                return GetNeighbours(newCenter, 0, 2, -1, 1, 0, 2);
            case 5:
                return GetNeighbours(newCenter, -1, 1, -1, 2, 0, 2);
            case 4:
                return GetNeighbours(newCenter, -1, 2, -1, 2, 0, 2);
            case 3:
                return GetNeighbours(newCenter, 0, 2, -1, 2, 0, 2);
            case 2:
                return GetNeighbours(newCenter, -1, 1, 0, 2, 0, 2);
            case 1:
                return GetNeighbours(newCenter, -1, 2, 0, 2, 0, 2);
            case 0:
                return GetNeighbours(newCenter, 0, 2, 0, 2, 0, 2);  
            default:
                return new IcosidodecahedronList();
        }
    }

    /// <summary>
    /// Get the icosidodechaedron removed from de cube render in the change center.
    /// </summary>
    /// <param name="center">The new center.</param>
    /// <param name="xmin">Min position in x to select a subcube.</param>
    /// <param name="xmax">Max position in x to select a subcube.</param>
    /// <param name="zmin">Min position in z to select a subcube.</param>
    /// <param name="zmax">Max position in z to select a subcube.</param>
    /// <param name="ymin">Min position in y to select a subcube.</param>
    /// <param name="ymax">Max position in y to select a subcube.</param>
    /// <returns>Return a list with the icosidodecahedron inside of a subcube.</returns>
    private IcosidodecahedronList GetNeighbours(Vector3 center, int xmin, int xmax, int zmin, int zmax, int ymin, int ymax) {
        IcosidodecahedronList icosiToPut = new IcosidodecahedronList();
        List<Vector3> allNeighboursPos = new List<Vector3>();
        List<Vector3> neighboursPosToRemove = new List<Vector3>();

        for (int y = -1; y < 2; y++)
            for (int z = -1; z < 2; z++)
                for (int x = -1; x < 2; x++)
                    allNeighboursPos.Add(center + new Vector3(x * _sizeCube, y * _sizeCube, z * _sizeCube));

        for (int y = ymin; y < ymax; y++)
            for (int z = zmin; z < zmax; z++)
                for (int x = xmin; x < xmax; x++)
                    neighboursPosToRemove.Add(center + new Vector3(x * _sizeCube, y * _sizeCube, z * _sizeCube));

        foreach (Vector3 elem in neighboursPosToRemove)
            allNeighboursPos.Remove(elem);

        foreach (Vector3 elem in allNeighboursPos) {
            IcosidodecahedronList temp;
            if (_icosiSaved.TryGetValue(elem, out temp)) {
                icosiToPut.AddRange(temp);
                _icosiSaved.Remove(elem);
            }
        }        
        return icosiToPut;
    }

    /// <summary>
    /// Get the local position in x using a global value.
    /// </summary>
    /// <param name="xPos">X global value.</param>
    /// <returns>The x local position.</returns>
    private int PlaneX(float xPos) {
        if (xPos < (_corner.x + _sizeCube))
            return 0;
        else if ((xPos >= (_corner.x + _sizeCube)) && (xPos < (_corner.x + (_sizeCube * 2))))
            return 1;
        else
            return 2;
    }

    /// <summary>
    /// Get the local position in y using a global value.
    /// </summary>
    /// <param name="yPos">Y global value.</param>
    /// <returns>The y local position.</returns>
    private int PlaneY(float yPos) {
        if (yPos < (_corner.y + _sizeCube))
            return 0;
        else if ((yPos >= (_corner.y + _sizeCube)) && (yPos < (_corner.y + (_sizeCube * 2))))
            return 1;
        else
            return 2;
    }

    /// <summary>
    /// Get the local position in z using a global value.
    /// </summary>
    /// <param name="zPos">Z global value.</param>
    /// <returns>The z local position.</returns>
    private int PlaneZ(float zPos) {
        if (zPos < (_corner.z + _sizeCube))
            return 0;
        else if ((zPos >= (_corner.z + _sizeCube)) && (zPos < (_corner.z + (_sizeCube * 2))))
            return 1;
        else
            return 2;
    }
}
