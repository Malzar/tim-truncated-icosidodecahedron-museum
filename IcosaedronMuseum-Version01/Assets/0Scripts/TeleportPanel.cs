﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// Class responsible to create a teleport panel.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class TeleportPanel : MonoBehaviour {

    private List<int> _icosiNumbers;
    private int _numOfGameObjects;
    private bool _isChange;
    private RectTransform _container;
    private RectTransform _labelPrefab;

    public GameObject _itemPrefab;

    /// <summary>
    /// Unity function. Initialize parameters
    /// </summary>
    void Awake() {
        _icosiNumbers = new List<int>();
        _numOfGameObjects = 0;
        _isChange = false;
    }

    /// <summary>
    /// Unity function. Main loop.
    /// </summary>
    void Update() {
        if (_isChange) {
            UpdateList();
            _isChange = false;
        }            
    }

    /// <summary>
    /// Clear the list of icosidodecahedron and add new ones.
    /// </summary>
    /// <param name="icosiToTeleport">List with the number of the icosidodecahedrons.</param>
    public void AddIcosiToTeleport(List<int> icosiToTeleport) {
        if(_icosiNumbers.Count>0)
            _icosiNumbers.Clear();
        icosiToTeleport.Sort();
        foreach (int elem in icosiToTeleport)
            _icosiNumbers.Add(elem);
        _isChange = true;
    }

    /// <summary>
    /// Update the list of memeber of telepor panel.
    /// </summary>
    private void UpdateList() {
        foreach (Transform elem in this.transform)
            Destroy(elem.gameObject);

        _numOfGameObjects = 0;
        _container = transform.GetComponent<RectTransform>();
        _labelPrefab = _itemPrefab.GetComponent<RectTransform>();
        float offsetY = 2.0f;

        if (_container.rect.height < (_labelPrefab.rect.height * _icosiNumbers.Count))
            _container.sizeDelta = new Vector2(_container.rect.width, ((_labelPrefab.rect.height * _icosiNumbers.Count + offsetY * _icosiNumbers.Count)));

        foreach (int elem in _icosiNumbers) {
            GameObject label = AddLavel(elem);
            float y = -_container.rect.height / 2 + _labelPrefab.rect.height / 2 + _labelPrefab.rect.height * _numOfGameObjects + offsetY * _numOfGameObjects;
            label.transform.localPosition = new Vector3(0, y, 0);
            label.transform.localScale = new Vector3(1f, 1f, 1f);
            _numOfGameObjects++;
        }
    }

    /// <summary>
    /// Set the actual icosidodecahedron in the panel.
    /// </summary>
    /// <param name="icosiNumber">The name of the platform.</param>
    public void SetActualIcosi(string platformName) {
        platformName = platformName.Substring(platformName.IndexOf('_') + 1);
        platformName = platformName.Remove(platformName.IndexOf('_'));
        this.transform.parent.parent.FindChild("CurrentIcosi").FindChild("Number").GetComponent<Text>().text = platformName;
    }

    /// <summary>
    /// Add new lavel into the teleport panel.
    /// </summary>
    /// <param name="icosiNumber">The number of the icosidodecahedron to add into instantiate lavel.</param>
    /// <returns>Return a game object with the parameters of the icosidodecahedron.</returns>
    private GameObject AddLavel(int icosiNumber){
        GameObject button = Instantiate(_itemPrefab) as GameObject;
        button.name = "Tp" + icosiNumber;
        button.transform.SetParent(gameObject.transform);
        button.transform.FindChild("Number").GetComponent<Text>().text=icosiNumber.ToString();
        button.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
        return button;
    }

    /// <summary>
    /// Teleport to the icosidodecahedron selected into lavel.
    /// </summary>
    public void Teleport() {
        GameObject.Find("Core").GetComponent<Core>().Teleport(int.Parse(this.gameObject.GetComponent<Text>().text));
    }
}
