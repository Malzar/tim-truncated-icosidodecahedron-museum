using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// This class indicated the faces of icosidodecahedron to use.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class InternalRoadGenerator {

    private List<int> _allFaces;
    private List<int> _availableFaces;
    private List<int> _inaccessibleFaces;
    private List<int> _tempBorder;
    private int _actualFace;
    private int _inceptionFace;
    private int _borderFace;
    private List<int> _road;
    private int _seed;

    /// <summary>
    /// Builder for the first icosidodecahedron.
    /// </summary>
    /// <param name="inceptionFace">The number of the inception face.</param>
    /// <param name="seed">The number to put at random seed.</param>
    public InternalRoadGenerator(int inceptionFace, int seed) {
        //inicializate all variables and add one face
        _allFaces = new List<int>();
        _availableFaces = new List<int>();
        _allFaces.Add(inceptionFace);
        _availableFaces.Add(inceptionFace);
        _actualFace = inceptionFace;
        _inceptionFace = inceptionFace;
        //to disable the face which is connected to the existent face
        _inaccessibleFaces = new List<int>();
        _road = new List<int>();
        _borderFace = 0;
        _seed = seed;
    }

    /// <summary>
    /// Builder.
    /// </summary>
    /// <param name="inceptionFace">The number of the inception face.</param>
    /// <param name="seed">The number to put at random seed.</param>
    /// <param name="borderFace">The number of the border face of inception face.</param>
    public InternalRoadGenerator(int inceptionFace, int seed, int borderFace) {
        //inicializate all variables and add one face
        _allFaces = new List<int>();
        _availableFaces = new List<int>();
        _allFaces.Add(inceptionFace);
        _availableFaces.Add(inceptionFace);
        _actualFace = inceptionFace;
        _inceptionFace = _actualFace;
        //to disable the face which is connected to the existent face
        _inaccessibleFaces = new List<int>();
        _road = new List<int>();
        _inaccessibleFaces.Add(borderFace);
        _borderFace = borderFace;
        _seed = seed;
    }

    /// <summary>
    /// Destroyer of icosidodecahedron.
    /// </summary>
    public void Destroy() {
        _allFaces.Clear();
        _availableFaces.Clear();
        _inaccessibleFaces.Clear();
        _allFaces = null;
        _availableFaces = null;
        _inaccessibleFaces = null;
    }

    /// <summary>
    /// Check the face to determine if all neighbours faces are used.
    /// </summary>
    /// <param name="baseFace">The number of face to check.</param>
    /// <returns>Return True if all neighbours are used, false in other case.</returns>
    private bool FaceIsComplete(int baseFace) {
        bool isComplete = false;

        if (baseFace == _inceptionFace)
            _allFaces.Add(_borderFace);

        switch (baseFace) {
            case 1:
                if (_allFaces.Contains(2))
                    if (_allFaces.Contains(3))
                        if (_allFaces.Contains(4))
                            isComplete = true;
                break;
            case 2:
                if (_allFaces.Contains(1))
                    if (_allFaces.Contains(5))
                        if (_allFaces.Contains(6))
                            isComplete = true;
                break;
            case 3:
                if (_allFaces.Contains(1))
                    if (_allFaces.Contains(7))
                        if (_allFaces.Contains(8))
                            isComplete = true;
                break;
            case 4:
                if (_allFaces.Contains(1))
                    if (_allFaces.Contains(9))
                        if (_allFaces.Contains(10))
                            isComplete = true;
                break;
            case 5:
                if (_allFaces.Contains(2))
                    if (_allFaces.Contains(10))
                        if (_allFaces.Contains(11))
                            isComplete = true;
                break;
            case 6:
                if (_allFaces.Contains(2))
                    if (_allFaces.Contains(7))
                        if (_allFaces.Contains(12))
                            isComplete = true;
                break;
            case 7:
                if (_allFaces.Contains(3))
                    if (_allFaces.Contains(6))
                        if (_allFaces.Contains(13))
                            isComplete = true;
                break;
            case 8:
                if (_allFaces.Contains(3))
                    if (_allFaces.Contains(9))
                        if (_allFaces.Contains(14))
                            isComplete = true;
                break;
            case 9:
                if (_allFaces.Contains(4))
                    if (_allFaces.Contains(8))
                        if (_allFaces.Contains(15))
                            isComplete = true;
                break;
            case 10:
                if (_allFaces.Contains(4))
                    if (_allFaces.Contains(5))
                        if (_allFaces.Contains(16))
                            isComplete = true;
                break;
            case 11:
                if (_allFaces.Contains(5))
                    if (_allFaces.Contains(12))
                        if (_allFaces.Contains(17))
                            isComplete = true;
                break;
            case 12:
                if (_allFaces.Contains(6))
                    if (_allFaces.Contains(11))
                        if (_allFaces.Contains(18))
                            isComplete = true;
                break;
            case 13:
                if (_allFaces.Contains(7))
                    if (_allFaces.Contains(14))
                        if (_allFaces.Contains(18))
                            isComplete = true;
                break;
            case 14:
                if (_allFaces.Contains(8))
                    if (_allFaces.Contains(13))
                        if (_allFaces.Contains(19))
                            isComplete = true;
                break;
            case 15:
                if (_allFaces.Contains(9))
                    if (_allFaces.Contains(16))
                        if (_allFaces.Contains(19))
                            isComplete = true;
                break;
            case 16:
                if (_allFaces.Contains(10))
                    if (_allFaces.Contains(15))
                        if (_allFaces.Contains(17))
                            isComplete = true;
                break;
            case 17:
                if (_allFaces.Contains(11))
                    if (_allFaces.Contains(16))
                        if (_allFaces.Contains(20))
                            isComplete = true;
                break;
            case 18:
                if (_allFaces.Contains(12))
                    if (_allFaces.Contains(13))
                        if (_allFaces.Contains(20))
                            isComplete = true;
                break;
            case 19:
                if (_allFaces.Contains(14))
                    if (_allFaces.Contains(15))
                        if (_allFaces.Contains(20))
                            isComplete = true;
                break;
            case 20:
                if (_allFaces.Contains(17))
                    if (_allFaces.Contains(18))
                        if (_allFaces.Contains(19))
                            isComplete = true;
                break;
            default:
                Debug.LogError("CheckIsComplete: Incorrect num of face: " + baseFace);
                break;
        }

        if (baseFace == _inceptionFace)
            _allFaces.Remove(_borderFace);
        return isComplete;
    }

    /// <summary>
    /// Get a random face of available face list.
    /// </summary>
    /// <returns>Return true if can get one, false in other case.</returns>
    private bool AddFaceRandom() {
        bool canPut = true;
        Random.seed = _seed;
        List<int> faces;

        _actualFace = _availableFaces[Random.Range(0, _availableFaces.Count - 1)];
        faces = FacesToUse(_actualFace);
        if (faces.Count == 0) {
            if (_availableFaces.Count > 0) {
                _actualFace = _availableFaces[Random.Range(0, _availableFaces.Count - 1)];
                _tempBorder = FacesToUse(_actualFace);
                if (_tempBorder.Count == 0) {
                    for (int i = 0; i < _availableFaces.Count; i++) {
                        _tempBorder = FacesToUse(_availableFaces[i]);
                        if (_tempBorder.Count > 0) {
                            _actualFace = _availableFaces[i];
                            break;
                        }
                    }
                }
            }
        }

        switch (faces.Count) {
            case 0:
                canPut = false;
                break;
            case 1:
                _allFaces.Add(faces[0]);
                _availableFaces.Add(faces[0]);
                break;
            default:
                int faceToAdd = Random.Range(0, faces.Count - 1);
                _allFaces.Add(faces[faceToAdd]);
                _availableFaces.Add(faces[faceToAdd]);
                break;
        }
        if (canPut) {
            //Delete faces which is completed 
            for (int i = _availableFaces.Count - 1; i >= 0; i--) {
                if (FaceIsComplete(_availableFaces[i]))
                    _availableFaces.RemoveAt(i);
            }
        }
        return canPut;
    }

    /// <summary>
    /// Get a face of available face list in FaceComplete mode.
    /// </summary>
    /// <returns>Return true if can get one, false in other case.</returns>
    private bool AddFaceComplete() {
        bool canPut = true;
        Random.seed = _seed;

        if (_tempBorder == null || _tempBorder.Count == 0) {
            _actualFace = _availableFaces[0];
            _tempBorder = FacesToUse(_actualFace);
            if (_tempBorder.Count == 0) {
                for (int i = 1; i < _availableFaces.Count; i++) {
                    _tempBorder = FacesToUse(_availableFaces[i]);
                    if (_tempBorder.Count > 0) {
                        _actualFace = _availableFaces[i];
                        break;
                    }
                }
            }
        }

        switch (_tempBorder.Count) {
            case 0:
                canPut = false;
                break;
            case 1:
                _allFaces.Add(_tempBorder[0]);
                _availableFaces.Add(_tempBorder[0]);
                _tempBorder.RemoveAt(0);
                break;
            default:
                _allFaces.Add(_tempBorder[0]);
                _availableFaces.Add(_tempBorder[0]);
                _tempBorder.RemoveAt(0);
                break;
        }
        if (canPut) {
            for (int i = _availableFaces.Count - 1; i >= 0; i--) {
                if (FaceIsComplete(_availableFaces[i]))
                    _availableFaces.RemoveAt(i);
            }
        }
        return canPut;
    }

    /// <summary>
    /// Get a face of available face list in LinealFace mode.
    /// </summary>
    /// <returns>Return true if can get one, false in other case.</returns>
    private bool AddFaceLineal() {
        bool canPut = true;

        Random.seed = _seed;

        _tempBorder = FacesToUse(_actualFace);
        if (_tempBorder.Count == 0) {
            if (_availableFaces.Count > 0) {
                _actualFace = _availableFaces[Random.Range(0, _availableFaces.Count - 1)];
                _tempBorder = FacesToUse(_actualFace);
                if (_tempBorder.Count == 0) {
                    for (int i = 0; i < _availableFaces.Count; i++) {
                        _tempBorder = FacesToUse(_availableFaces[i]);
                        if (_tempBorder.Count > 0) {
                            _actualFace = _availableFaces[i];
                            break;
                        }
                    }
                }
            }
        }

        switch (_tempBorder.Count) {
            case 0:
                canPut = false;
                break;
            case 1:
                _allFaces.Add(_tempBorder[0]);
                _availableFaces.Add(_tempBorder[0]);
                _actualFace = _tempBorder[0];
                break;
            default:
                int faceToAdd = Random.Range(0, _tempBorder.Count - 1);
                _allFaces.Add(_tempBorder[faceToAdd]);
                _availableFaces.Add(_tempBorder[faceToAdd]);
                _actualFace = _tempBorder[faceToAdd];
                break;
        }
        if (canPut) {
            for (int i = _availableFaces.Count - 1; i >= 0; i--) {
                if (FaceIsComplete(_availableFaces[i]))
                    _availableFaces.RemoveAt(i);
            }
        }
        return canPut;
    }

    /// <summary>
    /// Create a list with faces using the random mode.
    /// </summary>
    /// <param name="numFaces">Number of face to use.</param>
    /// <returns>Return a integer list with the indicated number of faces if is possible.</returns>
    private List<int> GetRoadRandom(int numFaces) {

        for (int i = 0; i < numFaces - 1; i++)
            if (!AddFaceRandom()) break;
        //remove inaccessible element
        if (_inaccessibleFaces.Count != 0) {
            foreach (int elem in _inaccessibleFaces) _allFaces.Remove(elem);
        }
        //sort list
        _allFaces.Sort();
        return _allFaces;
    }

    /// <summary>
    /// Create a list with faces using the linial mode.
    /// </summary>
    /// <param name="numFaces">Number of face to use.</param>
    /// <returns>Return a integer list with the indicated number of faces if is possible.</returns>
    private List<int> GetRoadLineal(int numFaces) {
        int count = 0;
        for (int i = 0; i < numFaces - 1; i++) {
            if (!AddFaceLineal()) {
                numFaces = count;
                break;
            }
            count++;
        }
        //remove inaccessible element
        if (_inaccessibleFaces.Count != 0) {
            foreach (int elem in _inaccessibleFaces) _allFaces.Remove(elem);
        }
        //sort list
        _allFaces.Sort();
        return _allFaces;
    }

    /// <summary>
    /// Create a list with faces using the faceComple mode.
    /// </summary>
    /// <param name="numFaces">Number of face to use.</param>
    /// <returns>Return a integer list with the indicated number of faces if is possible.</returns>
    private List<int> GetRoadComplete(int numFaces) {
        int count = 0;
        for (int i = 0; i < numFaces - 1; i++) {
            if (!AddFaceComplete()) {
                numFaces = count;
                break;
            }
            count++;
        }
        //remove inaccessible element
        if (_inaccessibleFaces != null) {
            foreach (int elem in _inaccessibleFaces) _allFaces.Remove(elem);
        }
        //sort list
        _allFaces.Sort();
        return _allFaces;
    }

    /// <summary>
    /// Get the border faces from especific face.
    /// </summary>
    /// <param name="baseFace">Number of face to use.</param>
    /// <param name="withBorder">Habilitate icosidodecahedron border face.</param>
    /// <returns>Return a list with the border faces.</returns>
    public List<int> GetFacesBorder(int baseFace, bool withBorder) {
        List<int> faces = new List<int>();

        if (withBorder)
            if (baseFace != _inceptionFace)
                _allFaces.Remove(_borderFace);

        switch (baseFace) {
            case 1:
                if (!_allFaces.Contains(2))
                    faces.Add(2);
                if (!_allFaces.Contains(3))
                    faces.Add(3);
                if (!_allFaces.Contains(4))
                    faces.Add(4);
                break;
            case 2:
                if (!_allFaces.Contains(1))
                    faces.Add(1);
                if (!_allFaces.Contains(5))
                    faces.Add(5);
                if (!_allFaces.Contains(6))
                    faces.Add(6);
                break;
            case 3:
                if (!_allFaces.Contains(1))
                    faces.Add(1);
                if (!_allFaces.Contains(7))
                    faces.Add(7);
                if (!_allFaces.Contains(8))
                    faces.Add(8);
                break;
            case 4:
                if (!_allFaces.Contains(1))
                    faces.Add(1);
                if (!_allFaces.Contains(9))
                    faces.Add(9);
                if (!_allFaces.Contains(10))
                    faces.Add(10);
                break;
            case 5:
                if (!_allFaces.Contains(2))
                    faces.Add(2);
                if (!_allFaces.Contains(10))
                    faces.Add(10);
                if (!_allFaces.Contains(11))
                    faces.Add(11);
                break;
            case 6:
                if (!_allFaces.Contains(2))
                    faces.Add(2);
                if (!_allFaces.Contains(7))
                    faces.Add(7);
                if (!_allFaces.Contains(12))
                    faces.Add(12);
                break;
            case 7:
                if (!_allFaces.Contains(3))
                    faces.Add(3);
                if (!_allFaces.Contains(6))
                    faces.Add(6);
                if (!_allFaces.Contains(13))
                    faces.Add(13);
                break;
            case 8:
                if (!_allFaces.Contains(3))
                    faces.Add(3);
                if (!_allFaces.Contains(9))
                    faces.Add(9);
                if (!_allFaces.Contains(14))
                    faces.Add(14);
                break;
            case 9:
                if (!_allFaces.Contains(4))
                    faces.Add(4);
                if (!_allFaces.Contains(8))
                    faces.Add(8);
                if (!_allFaces.Contains(15))
                    faces.Add(15);
                break;
            case 10:
                if (!_allFaces.Contains(4))
                    faces.Add(4);
                if (!_allFaces.Contains(5))
                    faces.Add(5);
                if (!_allFaces.Contains(16))
                    faces.Add(16);
                break;
            case 11:
                if (!_allFaces.Contains(5))
                    faces.Add(5);
                if (!_allFaces.Contains(12))
                    faces.Add(12);
                if (!_allFaces.Contains(17))
                    faces.Add(17);
                break;
            case 12:
                if (!_allFaces.Contains(6))
                    faces.Add(6);
                if (!_allFaces.Contains(11))
                    faces.Add(11);
                if (!_allFaces.Contains(18))
                    faces.Add(18);
                break;
            case 13:
                if (!_allFaces.Contains(7))
                    faces.Add(7);
                if (!_allFaces.Contains(14))
                    faces.Add(14);
                if (!_allFaces.Contains(18))
                    faces.Add(18);
                break;
            case 14:
                if (!_allFaces.Contains(8))
                    faces.Add(8);
                if (!_allFaces.Contains(13))
                    faces.Add(13);
                if (!_allFaces.Contains(19))
                    faces.Add(19);
                break;
            case 15:
                if (!_allFaces.Contains(9))
                    faces.Add(9);
                if (!_allFaces.Contains(16))
                    faces.Add(16);
                if (!_allFaces.Contains(19))
                    faces.Add(19);
                break;
            case 16:
                if (!_allFaces.Contains(10))
                    faces.Add(10);
                if (!_allFaces.Contains(15))
                    faces.Add(15);
                if (!_allFaces.Contains(17))
                    faces.Add(17);
                break;
            case 17:
                if (!_allFaces.Contains(11))
                    faces.Add(11);
                if (!_allFaces.Contains(16))
                    faces.Add(16);
                if (!_allFaces.Contains(20))
                    faces.Add(20);
                break;
            case 18:
                if (!_allFaces.Contains(12))
                    faces.Add(12);
                if (!_allFaces.Contains(13))
                    faces.Add(13);
                if (!_allFaces.Contains(20))
                    faces.Add(20);
                break;
            case 19:
                if (!_allFaces.Contains(14))
                    faces.Add(14);
                if (!_allFaces.Contains(15))
                    faces.Add(15);
                if (!_allFaces.Contains(20))
                    faces.Add(20);
                break;
            case 20:
                if (!_allFaces.Contains(17))
                    faces.Add(17);
                if (!_allFaces.Contains(18))
                    faces.Add(18);
                if (!_allFaces.Contains(19))
                    faces.Add(19);
                break;
            default:
                Debug.LogError("FacesToUse: Incorrect number of face: " + baseFace);
                break;
        }
        if (withBorder) {
            if (faces.Contains(_borderFace) && baseFace == _inceptionFace)
                faces.Remove(_borderFace);
            else 
                _allFaces.Add(_borderFace);
        } else
            if (faces.Contains(_borderFace)) 
                faces.Remove(_borderFace);
        return faces;
    }

    /// <summary>
    /// Get the border faces can use for a specefic face.
    /// </summary>
    /// <param name="face">Number of the face to use.</param>
    /// <returns>Return a list with border faces useful.</returns>
    private List<int> FacesToUse(int face) {
        List<int> facesToUse = new List<int>();

        facesToUse = GetFacesBorder(face, false);

        for (int i = 0; i < facesToUse.Count; i++) {
            if (_inaccessibleFaces.Contains(facesToUse[i])) {
                facesToUse.RemoveAt(i);
                i--;
            }
        }
        return facesToUse;
    }

    /// <summary>
    /// Get the oposite face.
    /// </summary>
    /// <param name="baseFace">Number of the face to use.</param>
    /// <returns>Te integer number of the oposite face.</returns>
    public static int GetOppositeFace(int baseFace) {
        int opposite = 0;

        switch (baseFace) {
            case 1:
                opposite = 20;
                break;
            case 2:
                opposite = 19;
                break;
            case 3:
                opposite = 17;
                break;
            case 4:
                opposite = 18;
                break;
            case 5:
                opposite = 14;
                break;
            case 6:
                opposite = 15;
                break;
            case 7:
                opposite = 16;
                break;
            case 8:
                opposite = 11;
                break;
            case 9:
                opposite = 12;
                break;
            case 10:
                opposite = 13;
                break;
            case 11:
                opposite = 8;
                break;
            case 12:
                opposite = 9;
                break;
            case 13:
                opposite = 10;
                break;
            case 14:
                opposite = 5;
                break;
            case 15:
                opposite = 6;
                break;
            case 16:
                opposite = 7;
                break;
            case 17:
                opposite = 3;
                break;
            case 18:
                opposite = 4;
                break;
            case 19:
                opposite = 2;
                break;
            case 20:
                opposite = 1;
                break;
            default:
                Debug.LogError("GetOppositeFace: Incorrect number of face: " + baseFace);
                break;
        }
        return opposite;
    }

    /// <summary>
    /// Create and get the road of face to use.
    /// </summary>
    /// <param name="numFaces">Number of faces to use.</param>
    /// <param name="modeComposition">Number of selection mode.</param>
    /// <returns>Return a list with the number of the face to use.</returns>
    public List<int> GetRoad(int numFaces, int modeComposition) {
        _road.Clear();
        switch (modeComposition) {
            case 1:
                _road.AddRange(GetRoadRandom(numFaces));
                break;
            case 2:
                _road.AddRange(GetRoadLineal(numFaces));
                break;
            default:
                _road.AddRange(GetRoadComplete(numFaces));
                break;
        }
        return _road;
    }

    /// <summary>
    /// Get the road of face to use.
    /// </summary>
    /// <returns></returns>
    public List<int> GetRoad() { return _road; }

    /// <summary>
    /// Get all faces without all neighbours used.
    /// </summary>
    /// <returns>Return a list with the number of the faces without all neigbours.</returns>
    public List<int> GetLimitFacesFaces() {
        if (_availableFaces.Count < 1) return null;
        else return _availableFaces;
    }

    /// <summary>
    /// Block faces to can not use.
    /// </summary>
    /// <param name="collidedFaces">A list with the number of the faces to block.</param>
    /// <returns>Return true if inception face is contained in the block faces, false in other case.</returns>
    public bool AddCollidedFaces(List<int> collidedFaces) {
        _inaccessibleFaces = _inaccessibleFaces.Union(collidedFaces).ToList();
        _actualFace = _inceptionFace;
        _availableFaces.Clear();
        _availableFaces.Add(_actualFace);
        _allFaces.Clear();
        _allFaces.Add(_actualFace);

        return !_inaccessibleFaces.Contains(_inceptionFace);
    }

    /// <summary>
    /// Get the inception face.
    /// </summary>
    /// <returns>Return the integer number of inception face.</returns>
    public int GetInceptionFace() { return _inceptionFace; }
    
    /// <summary>
    /// Get a random useful face.
    /// </summary>
    /// <returns>Return a integer with the number of a face.</returns>
    public int GetRandomFace() {
        int face;
        bool find;

        do {
            face = _availableFaces[Random.Range(0, _availableFaces.Count - 1)];
            find = _inaccessibleFaces.Contains(face);
        } while (find);

        return face;
    }
}