﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class is added to face's gameobject for the purpose of attrack the player.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class GravityAttractor : MonoBehaviour {

    private float _gravity = -9.8f;
    private bool _wait = false;

    /// <summary>
    /// Generate the attraction to player.
    /// </summary>
    /// <param name="body">Transform of player's game object</param>
    public void Attract(Transform body) {
        if (!_wait) {
            if (transform.name.Contains("PI")) {
                Vector3 targetDir = (body.position - transform.parent.position).normalized;

                body.rotation = Quaternion.FromToRotation(-body.up, targetDir) * body.rotation;
                body.rigidbody.AddForce(targetDir * -_gravity);
            } else if (transform.name.Contains("PE")) {
                Vector3 targetDir = (body.position - transform.parent.position).normalized;

                body.rotation = Quaternion.FromToRotation(body.up, targetDir) * body.rotation;
                body.rigidbody.AddForce(targetDir * _gravity);
            }
        }
    }

    /// <summary>
    /// Freeze gravity attraction.
    /// </summary>
    public void Stop() {
        _wait = true;
    }

    /// <summary>
    /// Unfreeze gravity atracttion.
    /// </summary>
    public void Play() {
        _wait = false;
    }
}