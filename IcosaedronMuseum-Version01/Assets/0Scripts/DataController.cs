﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// This class contains the controll of interface.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class DataController : MonoBehaviour {

    public GameObject _itemPrefab;

    /// <summary>
    /// Upadate the distance of representation with a scroll bar
    /// </summary>
    public void UpdateSizeValue() {
        float value = this.transform.FindChild("Scrollbar").gameObject.GetComponent<Scrollbar>().value;

        int valueFixed = (int)(value * 9);
        valueFixed = valueFixed * 3 + 153;
        this.gameObject.GetComponent<Text>().text = "Size "+valueFixed.ToString();
        GameObject.Find("Data").GetComponent<Data>().AddSizeView(valueFixed);

        RectTransform rectBar = this.transform.FindChild("Scrollbar").FindChild("Bar").GetComponent<RectTransform>();
        float width = this.transform.FindChild("Scrollbar").GetComponent<RectTransform>().rect.width;
        rectBar.sizeDelta = new Vector2((float)width * value, 3);

        rectBar.localPosition = new Vector3((float)width * (value + 0.00001f) / 2 - (float)width / 2, rectBar.localPosition.y, rectBar.localPosition.z);        
    }

    /// <summary>
    /// Update the number of faces from a pattern object with a scroll bar
    /// </summary>
    public void UpdateNumFacesValue() {
        float value = this.transform.FindChild("Scrollbar").gameObject.GetComponent<Scrollbar>().value;

        int valueFixed = (int)(value * 18);
        valueFixed = valueFixed + 1;
        this.gameObject.GetComponent<Text>().text = valueFixed.ToString();

        string number = this.transform.parent.parent.parent.name;
        int pos = int.Parse(number.Substring(number.IndexOf("_") + 1));

        GameObject.Find("Data").GetComponent<Data>().AddNumberFace(pos, valueFixed);

        RectTransform rectBar = this.transform.FindChild("Scrollbar").FindChild("Bar").GetComponent<RectTransform>();
        float width = this.transform.FindChild("Scrollbar").GetComponent<RectTransform>().rect.width;
        rectBar.sizeDelta = new Vector2((float) width*value,3);

        rectBar.localPosition = new Vector3((float)width * (value + 0.00001f) / 2 - (float)width/2, rectBar.localPosition.y, rectBar.localPosition.z);
    }

    /// <summary>
    /// Evaluate an inmput field to seed value.
    /// </summary>
    public void UpdateSeedValue() {
        int value;
        string input =this.gameObject.GetComponent<InputField>().text;

        if (!int.TryParse(input, out value) && (input != "")) {
            if (input.Length > 9) {
                this.transform.FindChild("ErrorSize").gameObject.SetActive(true);
                this.transform.FindChild("Error").gameObject.SetActive(false);
            } else {
                this.transform.FindChild("ErrorSize").gameObject.SetActive(false);
                this.transform.FindChild("Error").gameObject.SetActive(true);
            }
        } else {
            this.transform.FindChild("Error").gameObject.SetActive(false);
            this.transform.FindChild("ErrorSize").gameObject.SetActive(false);
        }
        
    }

    /// <summary>
    /// Evaluate an inmput field and add to seed value.
    /// </summary>
    public void SetSeedValue() {
        int value;
        string input =this.gameObject.GetComponent<InputField>().text;

        if (!int.TryParse(input, out value) && (input != "")) {
            if (input.Length > 9) {
                this.transform.FindChild("ErrorSize").gameObject.SetActive(true);
                this.transform.FindChild("Error").gameObject.SetActive(false);
            } else {
                this.transform.FindChild("ErrorSize").gameObject.SetActive(false);
                this.transform.FindChild("Error").gameObject.SetActive(true);
            }
        } else {
            this.transform.FindChild("Error").gameObject.SetActive(false);
            this.transform.FindChild("ErrorSize").gameObject.SetActive(false);
            GameObject.Find("Data").GetComponent<Data>().AddSeed(value);
        }
    }

    /// <summary>
    /// Change value of face mode.
    /// </summary>
    public void SetFaceMode() {
        string number = this.transform.parent.parent.parent.parent.parent.name;
        int pos = int.Parse(number.Substring(number.IndexOf("_") + 1));

        if (this.transform.name.Contains("Random"))
            GameObject.Find("Data").GetComponent<Data>().AddFaceMode(pos, 1);
        if (this.transform.name.Contains("Lineal"))
            GameObject.Find("Data").GetComponent<Data>().AddFaceMode(pos, 2);
        if (this.transform.name.Contains("FullFace"))
            GameObject.Find("Data").GetComponent<Data>().AddFaceMode(pos, 3);
    }

    /// <summary>
    /// Update value put all faces from pattern object
    /// </summary>
    public void UpdatePutAllFaces() {
        string number = this.transform.parent.parent.name;
        int pos = int.Parse(number.Substring(number.IndexOf("_") + 1));
        GameObject.Find("Data").GetComponent<Data>().AddPutAllFace(pos, this.gameObject.GetComponent<Toggle>().isOn);
    }

    /// <summary>
    /// Quit the pattern object from scene
    /// </summary>
    public void RemovePattern() {
        if(this.transform.parent.parent.parent.transform.childCount>1){
            string number = this.transform.parent.parent.name;
            int pos = int.Parse(number.Substring(number.IndexOf("_") + 1));
        
            GameObject.Find("Data").GetComponent<Data>().RemovePattern(pos);
            Destroy(this.transform.parent.parent.gameObject);

            RectTransform pattern = _itemPrefab.GetComponent<RectTransform>();
            foreach (Transform child in this.transform.parent.parent.parent.transform) {
                int patternPos = int.Parse(child.name.Substring(child.name.IndexOf("_") + 1));
                if (patternPos > pos) {
                    child.name = "Pattern_" + (patternPos - 1).ToString();
                    child.transform.FindChild("Panel").FindChild("Text").GetComponent<Text>().text = "Pattern " + patternPos.ToString();
                    RectTransform patRect = child.GetComponent<RectTransform>();
                    patRect.anchoredPosition = new Vector2(pattern.rect.width * (patternPos - 1) + 5 * (patternPos), -(pattern.rect.height / 2));
                    patRect.offsetMin = new Vector2(patRect.offsetMin.x, 0);
                    patRect.offsetMax = new Vector2(patRect.offsetMax.x, 0);
                }
            }
            RectTransform container = GameObject.Find("PatternScroll").GetComponent<RectTransform>();
            container.sizeDelta = new Vector2(container.rect.width-pattern.rect.width-10, container.rect.height);
        }
    }

    /// <summary>
    /// Create a new prefab pattern into scene
    /// </summary>
    public void AddPattern() {
        int pos = GameObject.Find("PatternScroll").transform.childCount;
        GameObject.Find("Data").GetComponent<Data>().AddFaceMode(1);
        GameObject.Find("Data").GetComponent<Data>().AddNumberFace(1);
        GameObject.Find("Data").GetComponent<Data>().AddPutAllFaces(true);
        
        float width = _itemPrefab.GetComponent<RectTransform>().rect.width;
        float height = _itemPrefab.GetComponent<RectTransform>().rect.height;
        RectTransform container = GameObject.Find("PatternScroll").GetComponent<RectTransform>();
        container.sizeDelta = new Vector2(((width+10) * (pos+1)), 0);

        GameObject pattern = Instantiate(_itemPrefab) as GameObject;
        pattern.name = "Pattern_" + pos.ToString();
        pattern.transform.SetParent(GameObject.Find("PatternScroll").transform);
        pattern.transform.FindChild("Panel").FindChild("Text").GetComponent<Text>().text = "Pattern "+(pos+1).ToString();
        RectTransform patRect = pattern.gameObject.GetComponent<RectTransform>();
        patRect.localScale = new Vector3(1f, 1f, 1f);
        patRect.anchoredPosition = new Vector2(width * pos + 5 * (pos + 1), -(height / 2));
        patRect.offsetMin = new Vector2(patRect.offsetMin.x, 0);
        patRect.offsetMax = new Vector2(patRect.offsetMax.x, 0);
    }

    /// <summary>
    /// Load principal scene
    /// </summary>
    public void StarScene() {
        DontDestroyOnLoad(GameObject.Find("Data").gameObject);
        Application.LoadLevel("Principal");
    }

    /// <summary>
    /// Quit program
    /// </summary>
    public void Exit() {
        Application.Quit();
    }

    /// <summary>
    /// Load introduction scene
    /// </summary>
    public void ReturnIntro() {
        GameObject.Find("Data").GetComponent<Data>().Restart();
        Application.LoadLevel("Intro");
    }

    /// <summary>
    /// Hide menu from principal scene
    /// </summary>
    public void ReturnPrincipal() {
        GameObject.Find("Core").GetComponent<Core>().ReturnFromMenu();
    }
}
