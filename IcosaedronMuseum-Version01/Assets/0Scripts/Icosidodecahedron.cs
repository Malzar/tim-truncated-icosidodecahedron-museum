﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class controls an icosidodecahedron.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class Icosidodecahedron {

    private IcosidodecahedronBuilder _builder;
    private InternalRoadGenerator _road;
    private List<IcosidodecahedronUnion> _unions;
    public int _numFaces;
    private int _corridorFather;
    private int _faceLimitFather;
    private Vector3 _center;
    private int _modeComposition;
    private TupleList _facesBlock;
    private bool _putAllFaces;

    /// <summary>
    /// Builder with references to other icosidodecahedron.
    /// </summary>
    /// <param name="center"> The center of gameobject.</param>
    /// <param name="icosiNumber"> Number of icosidodecahedron.</param>
    /// <param name="faceMode"> Number of the mode of platform.</param>
    /// <param name="inceptionFace"> Number of the inception face.</param>
    /// <param name="borderFace"> Number of the border face.</param>
    /// <param name="fatherOrder"> Number of the number of icosidodecahedron father.</param>
    /// <param name="modeComposition"> Number of the positioning mode face.</param>
    /// <param name="seed"> Number of the seed aplies to random generation.</param>
    /// <param name="putAllFaces"> The choice to put or not all faces.</param>
    public Icosidodecahedron(Vector3 center, int icosiNumber, int faceMode, int inceptionFace, int borderFace, int fatherOrder, int modeComposition, int seed, bool putAllFaces) {
        _builder = new IcosidodecahedronBuilder(center, icosiNumber, faceMode, fatherOrder);
        _road = new InternalRoadGenerator(inceptionFace, seed, borderFace);
        _unions = new List<IcosidodecahedronUnion>();
        _center = center;
        _modeComposition = modeComposition;
        _facesBlock = new TupleList(icosiNumber);
        _putAllFaces = putAllFaces;
    }

    /// <summary>
    /// Builder without references to other icosidodecahedron.
    /// </summary>
    /// <param name="center"> The center of gameobject.</param>
    /// <param name="icosiNumber"> Number of the number of icosidodecahedron.</param>
    /// <param name="faceMode"> Number of the mode of platform.</param>
    /// <param name="inceptionFace"> Number of the inception face.</param>
    /// <param name="modeComposition"> Number of the positioning mode face.</param>
    /// <param name="seed"> Number of the seed aplies to random generation.</param>
    /// <param name="putAllFaces"> The choice to put or not all faces.</param>
    public Icosidodecahedron(Vector3 center, int icosiNumber, int faceMode, int inceptionFace, int modeComposition, int seed, bool putAllFaces) {
        _builder = new IcosidodecahedronBuilder(center, icosiNumber, faceMode, 0);
        _road = new InternalRoadGenerator(inceptionFace, seed);
        _unions = new List<IcosidodecahedronUnion>();
        _center = center;
        _modeComposition = modeComposition;
        _facesBlock = new TupleList(icosiNumber);
        _putAllFaces = putAllFaces;
    }

    /// <summary>
    /// Destroyer.
    /// </summary>
    public void Destroy() {
        _unions.Clear();
        _unions = null;
        _road.Destroy();
        _builder.Destroy();
    }

    /// <summary>
    /// Instantiate an icosidodecahedron gameobject into scene with a number of faces.
    /// </summary>
    /// <param name="numFaces"></param>
    public void PutInToScene(int numFaces) {
        _numFaces = numFaces;
        List<int> internalRoad = _road.GetRoad(_numFaces, _modeComposition);
        foreach (int elem in internalRoad)
            _builder.AddFace(elem, true);
    }

    /// <summary>
    /// Istantiate an icosidodecahedron gameobject saved into the scene.
    /// </summary>
    public void PutInToScene() {
        List<int> road = _road.GetRoad();
        List<Tuple> facesBlock;

        _builder.ReCreate(_center);
        _builder.Rendered();

        foreach (int elem in road)
            _builder.AddFace(elem, false);
        facesBlock = _facesBlock.GetFaces();

        foreach (Tuple elem in facesBlock) {
            if (elem.GetThird() == -1)
                _builder.BlockFace(elem.GetFirst(), elem.GetSecond());
            else
                _builder.BlockFace(elem.GetFirst(), elem.GetSecond(), elem.GetThird());
        }
    }

    /// <summary>
    /// Generate all possible union from the icosidodecahedron.
    /// </summary>
    public void GenerateUnions() {      
        List<int> limitFaces = _road.GetLimitFacesFaces();

        for (int i = 0; i < limitFaces.Count; i++) {
            List<int> borderFaces = _road.GetFacesBorder(limitFaces[i], true);
        
            for (int j = 0; j < borderFaces.Count; j++) {
                IcosidodecahedronUnion union = new IcosidodecahedronUnion();
                union.SetLimitFace(limitFaces[i]);
                union.SetBorderFace(borderFaces[j]);
                union.SetCenter(_builder.GetNewCenterPoint(limitFaces[i], borderFaces[j]));
                union.SetInceptionMode(_builder.GetFaceMode());
                _unions.Add(union);
            }
        }
    }

    /// <summary>
    /// Destroy only the gameobject component from the icosidodecahedron.
    /// </summary>
    public void Hide() { _builder.Destroy(); }

    /// <summary>
    /// Get all possible union from the icosidodecahedron.
    /// </summary>
    /// <returns> Return a icosiodecahedronUnion list with all union of this icosidodecahedron.<see cref="IcosidodecahedronUnion"/></returns>
    public List<IcosidodecahedronUnion> GetUnion() { return _unions; }

    /// <summary>
    /// Get the number of the icosidodecahedron.
    /// </summary>
    /// <returns>Return the integer number of the icosidodecahedron</returns>
    public int GetNumber() { return _builder.GetNumber(); }

    /// <summary>
    /// Rebuild the icosidodecahedron with constraints.
    /// </summary>
    /// <param name="inaccesibleFaces">The faces whose can not use.</param>
    /// <returns>Return true if can reubid, false is not.</returns>
    public bool Rebuild(List<int> inaccesibleFaces) {
        bool canRebuild = true;

        if (!_road.AddCollidedFaces(inaccesibleFaces)) {
            canRebuild = false;
        } else {
            _builder.DestroyFaces();
            _unions.Clear();
            List<int> internalRoad = _road.GetRoad(_numFaces,_modeComposition);
            if (_putAllFaces) {
                if (internalRoad.Count != _numFaces) {
                    canRebuild = false;
                } else {
                    canRebuild = true;
                    foreach (int elem in internalRoad)
                        _builder.AddFace(elem, true);
                }
            } else {
                if (internalRoad.Count == 0) {
                    canRebuild = false;
                } else {
                    canRebuild = true;
                    foreach (int elem in internalRoad)
                        _builder.AddFace(elem, true);
                }
            }
        }
        return canRebuild;
    }

    /// <summary>
    /// Block a corridor to specific face.
    /// </summary>
    /// <param name="face">Number of face to edit</param>
    /// <param name="corridorA">Number of the corridor to block</param>
    public void BlockFace(int face, int corridorA) {
        _builder.BlockFace(face, corridorA);
        _facesBlock.Add(face, corridorA);
    }

    /// <summary>
    /// Block two corridor to specific face.
    /// </summary>
    /// <param name="face">Number of face to edit</param>
    /// <param name="corridorA">Number of the corridor to block</param>
    /// <param name="corridorB">Number of the corridor to block</param>
    public void BlockFace(int face, int corridorA, int corridorB) {
        _builder.BlockFace(face, corridorA, corridorB);
        _facesBlock.Add(face, corridorA, corridorB);
    }

    /// <summary>
    /// Get the number of icosidodecahedron father
    /// </summary>
    /// <returns>Return integer number of icosidodecahedron father</returns>
    public int GetFatherNumber() { return _builder.GetFatherNumber(); }

    /// <summary>
    /// Get the face inception
    /// </summary>
    /// <returns>Return interger number of inception face</returns>
    public int GetInceptionFace() { return _road.GetInceptionFace(); }

    /// <summary>
    /// Set the father parameters
    /// </summary>
    /// <param name="faceLimit">Number of the face inception of icosidodecahedron father</param>
    /// <param name="corridor">Number of the corridor</param>
    public void SetDataFather(int faceLimit, int corridor) {
        _faceLimitFather = faceLimit;
        _corridorFather = corridor;
    }

    /// <summary>
    /// Get the corridor used to connect the icosidodecahedron father and this icosidodecahedron
    /// </summary>
    /// <returns>Return the integer number of corridor</returns>
    public int GetCorridorFather() { return _corridorFather; }

    /// <summary>
    /// Get the limit face of icosidodecahedron father used to connect with this icosidodecahedron
    /// </summary>
    /// <returns>The integer number of the limit face</returns>
    public int GetFaceLimitFather() { return _faceLimitFather; }

    /// <summary>
    /// Get the center position of this icosidodecahedron
    /// </summary>
    /// <returns>Retunr a Vector3 position</returns>
    public Vector3 GetCenter() { return _center; }

    /// <summary>
    /// Unhide the faces of this icosidodecahedron 
    /// </summary>
    public void Render() { _builder.Rendered(); }

    /// <summary>
    /// Get random face object.
    /// </summary>
    /// <returns>Return a transform component from a platform gameobject</returns>
    public Transform GetRandomFace() { return _builder.GetFace(_road.GetRandomFace()); }

    /// <summary>
    /// Activate the proces to check collision.
    /// </summary>
    public void CheckFaces() { _builder.Checkfaces(); }
}
