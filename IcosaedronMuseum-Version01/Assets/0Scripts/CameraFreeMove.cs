﻿using UnityEngine;
using System.Collections;

public class CameraFreeMove : MonoBehaviour {
    float mouseSpeed = 1.0f;
    float fingerSpeed = 0.1f;
    bool activateDamping = true;
    float dampingTime = 0.25f;

    private float currentX;
    private float currentY;
    private float currentZ;

    private float targetX;
    private float targetY;
    private float targetZ;

    private float xVel;
    private float yVel;
    private float zVel;

    private Vector3 moveDirection;

    private Vector3 xDirection;
    private Vector3 zDirection;

    private bool isMobile = false;
    private bool _wait;

    void Start() {
        _wait = false;
        isMobile = (Application.platform == RuntimePlatform.IPhonePlayer) || (Application.platform == RuntimePlatform.Android);
    }

    void Update() {
        if (isMobile) {
            TouchMove();
        } else {
            MouseMove();
        }

        if (!_wait) {
            if (activateDamping) {
                currentX = Mathf.SmoothDamp(currentX, targetX, ref xVel, dampingTime);
                currentY = Mathf.SmoothDamp(currentY, targetY, ref yVel, dampingTime);
                currentZ = Mathf.SmoothDamp(currentZ, targetZ, ref zVel, dampingTime);
            } else {
                currentX = targetX;
                currentY = targetY;
                currentZ = targetZ;
            }

            TransformMovement();

            transform.position += moveDirection;
        }

    }

    private void SetPosition(Vector3 newPos) {
        transform.position = newPos;
    }

    private void TransformMovement() {
        xDirection = transform.right;
        xDirection.y = 0.0f;
        xDirection.Normalize();

        zDirection = transform.forward;
        zDirection.y = 0.0f;
        zDirection.Normalize();

        moveDirection = new Vector3(0, currentY, 0);
        moveDirection += xDirection * currentX;
        moveDirection += zDirection * currentZ;
    }

    private void MouseMove() {
        if (Input.GetMouseButton(1)) {
            targetX = Input.GetAxis("Mouse X") * mouseSpeed;
            targetZ = Input.GetAxis("Mouse Y") * mouseSpeed;
        } else targetX = targetZ = 0.0f;

        if (Input.GetMouseButton(2)) {
            targetY = Input.GetAxis("Mouse Y") * mouseSpeed;
        } else targetY = 0.0f;
    }

    private void TouchMove() {
        if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved) {
            // Get movement of the finger since last frame
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

            // Move object across XY plane
            targetX += touchDeltaPosition.x * fingerSpeed;
            targetZ -= touchDeltaPosition.y * fingerSpeed;
        } else targetX = targetZ = 0.0f;


        if (Input.touchCount == 3 && Input.GetTouch(0).phase == TouchPhase.Moved) {
            // Get movement of the finger since last frame
            Vector2 touchDeltaPositionB = Input.GetTouch(0).deltaPosition;

            // Move object across XY plane
            targetY += touchDeltaPositionB.x * fingerSpeed;
        } else targetY = 0.0f;
    }

    public void Stop() { _wait = true; }

    public void Play() { _wait = false; }
}