﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class contains the controll of player in first person.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class FirstPersonController : MonoBehaviour {

    public float _mouseSensitivityX = 250f;
    public float _mouseSensitivityY = 250f;
    public int _walkSpeed = 4;

    private Transform _cameraT;
    private Vector3 moveAmount;
    private Vector3 _smoothMoveVelocity;
    private float _verticalLookRotation;
    private bool _wait;
    private bool _delay;

    /// <summary>
    /// Unity function. Initialize parameters
    /// </summary>
    void Start() {
        _wait = false;
        _delay = false;
        _cameraT = this.transform.FindChild("Camera").transform;
    }

    /// <summary>
    /// Unity function. Main loop.
    /// </summary>
    void Update() {
        if (!_wait) {
            transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * Time.deltaTime * _mouseSensitivityX);
            _verticalLookRotation += Input.GetAxis("Mouse Y") * Time.deltaTime * _mouseSensitivityY;
            _verticalLookRotation = Mathf.Clamp(_verticalLookRotation, -60, 80);
            _cameraT.localEulerAngles = Vector3.left * _verticalLookRotation;

            Vector3 moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
            Vector3 targetMoveAmount = moveDir * _walkSpeed;
            moveAmount = Vector3.SmoothDamp(moveAmount, targetMoveAmount, ref _smoothMoveVelocity, .15f);
        }
        if (_delay) {
            _delay = false;
            _wait = false;
        }
    }

    /// <summary>
    /// Unity function. Second loop.
    /// </summary>
    void FixedUpdate() {
        if (!_wait) {
            Vector3 localMove = transform.TransformDirection(moveAmount) * Time.fixedDeltaTime;
            rigidbody.MovePosition(rigidbody.position + localMove);
        }
    }

    /// <summary>
    /// Set the game object to attract
    /// </summary>
    /// <param name="newPlatform">GameObject to atract</param>
    public void SetPlatform(GameObject newPlatform) {
        transform.GetComponent<GravityBody>().SetPlatform(newPlatform);
    }

    /// <summary>
    /// Freeze player control
    /// </summary>
    public void Stop() {       
        rigidbody.constraints = RigidbodyConstraints.FreezePosition;
        _wait = true; 
        
    }

    /// <summary>
    /// Unfreeze player control
    /// </summary>
    public void Play() {
        rigidbody.constraints = RigidbodyConstraints.None;
        rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        _delay = true;
    }
}
