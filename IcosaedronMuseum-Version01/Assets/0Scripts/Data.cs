﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class contains patterns data.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class Data : MonoBehaviour {
    private List<int> _numberOfFace;
    private List<int> _faceMode;
    private List<bool> _putAllFaces; 
    private int _seed;
    private int _sizeView;

    /// <summary>
    /// Unity function. Initialize parameters with default values and reserve memory
    /// </summary>
    void Awake(){
        _numberOfFace = new List<int>();
        _faceMode = new List<int>();
        _putAllFaces = new List<bool>();
        _numberOfFace.Add(1);
        _faceMode.Add(1);
        _putAllFaces.Add(true);
        _seed = 0;
        _sizeView = 153;
    }
    
    /// <summary>
    /// Replace the positioning mode face from especific pattern.
    /// </summary>
    /// <param name="pos"> Pattern number </param>
    /// <param name="mode"> Positioning mode face to replace</param>
    public void AddFaceMode(int pos, int mode) { _faceMode[pos] = mode; }

    /// <summary>
    /// Add a positioning mode face from new pattern;
    /// </summary>
    /// <param name="mode"> Positioning mode face to add</param>
    public void AddFaceMode(int mode) { _faceMode.Add(mode); }

    /// <summary>
    /// Replace the number of faces from especific pattern.
    /// </summary>
    /// <param name="pos"> Pattern number</param>
    /// <param name="number"> Number of faces to replace</param>
    public void AddNumberFace(int pos, int number) { _numberOfFace[pos] = number; }

    /// <summary>
    /// Add the number of faces from new pattern.
    /// </summary>
    /// <param name="number"> Number of faces to add</param>
    public void AddNumberFace(int number) { _numberOfFace.Add(number); }

    /// <summary>
    /// Add the choice to put or not all faces from new patter.
    /// </summary>
    /// <param name="pos"> Pattern number</param>
    /// <param name="putAllFaces"> Do you put all faces?</param>
    public void AddPutAllFace(int pos, bool putAllFaces) { _putAllFaces[pos] = putAllFaces; }

    /// <summary>
    /// Replace the choice to put or not all faces from especific pattern.
    /// </summary>
    /// <param name="putAllFaces"> Do you put all faces?</param>
    public void AddPutAllFaces(bool putAllFaces) { _putAllFaces.Add(putAllFaces); }

    /// <summary>
    /// Add a specific seed to generate random number in scene.
    /// </summary>
    /// <param name="seed">Seed of the random number</param>
    public void AddSeed(int seed) { _seed = seed; }

    /// <summary>
    /// Add a specific distance of representation 
    /// </summary>
    /// <param name="size">Size of view</param>
    public void AddSizeView(int size) { _sizeView = size; }

    /// <summary>
    /// Get face mode from all patterns.
    /// </summary>
    /// <returns> A integer list with face mode from each pattern</returns>
    public List<int> GetFaceMode() { return _faceMode; }

    /// <summary>
    /// Get the number of faces from all patterns.
    /// </summary>
    /// <returns> A integer list with number of faces from each pattern</returns>
    public List<int> GetNumberOfFaces() { return _numberOfFace; }

    /// <summary>
    /// Get the seed from a random number
    /// </summary>
    /// <returns> A integer number of seed</returns>
    public int GetSeed() { return _seed; }

    /// <summary>
    /// Get the distance of representation
    /// </summary>
    /// <returns> A integer value to representation</returns>
    public int GetSizeView() { return _sizeView; }

    /// <summary>
    /// Get the choice to put or not all faces from all patterns.
    /// </summary>
    /// <returns> A bolean list with choice from each pattern</returns>
    public List<bool> GetPutAllFaces() { return _putAllFaces; }
    
    /// <summary>
    /// Delete a specific pattern.
    /// </summary>
    /// <param name="pos">Pattern number</param>
    public void RemovePattern(int pos) {
        _numberOfFace.RemoveAt(pos);
        _faceMode.RemoveAt(pos);
        _putAllFaces.RemoveAt(pos);
    }

    /// <summary>
    /// Restart pattern data.
    /// </summary>
    public void Restart() {
        _faceMode.Clear();
        _numberOfFace.Clear();
        _putAllFaces.Clear();
        _numberOfFace.Add(1);
        _faceMode.Add(1);
        _putAllFaces.Add(true);
        _seed = 0;
        _sizeView = 153;
    }
}
