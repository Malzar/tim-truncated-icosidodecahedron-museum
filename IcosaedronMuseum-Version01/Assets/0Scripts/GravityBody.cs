﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class is added to player for the purpose of attrack player to a game object.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
[RequireComponent (typeof (Rigidbody))]
public class GravityBody : MonoBehaviour {

    private GravityAttractor _platform;

    /// <summary>
    /// Unity function. Initialize parameters
    /// </summary>
    void Start() {
        rigidbody.useGravity = false;
        rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    /// <summary>
    /// Unity function. Second loop.
    /// </summary>
    void FixedUpdate() {
        if(_platform!=null)
            _platform.Attract(transform);
    }

    /// <summary>
    /// Set the game object to attract
    /// </summary>
    /// <param name="newPlatform">GameObject to atract</param>
    public void SetPlatform(GameObject newPlatform) {
        _platform = newPlatform.GetComponent<GravityAttractor>();
    }
}
