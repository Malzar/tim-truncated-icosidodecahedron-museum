﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class responsible for saving all produced collisions
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class ColisionList{

	private class IcosiColision{

		private int _icosidodecahedron;
		private List <int> _faces;

		public IcosiColision(int nIcosi, int nFace){
			_icosidodecahedron = nIcosi;
			_faces = new List<int>();
			_faces.Add(nFace);
		}

		public int GetNumIcosi(){return _icosidodecahedron;}
		public List<int> GetFaces(){return _faces;}

		public void AddFace(int face){_faces.Add(face);}
		public void RemoveFaces(){ _faces.Clear();}
	}
	private List<IcosiColision> _icosiCollison;

    /// <summary>
    /// Builder, not need parametres
    /// </summary>
	public ColisionList(){
		_icosiCollison = new List<IcosiColision>();
	}

    /// <summary>
    /// Add new collision to the list.
    /// </summary>
    /// <param name="icosiNumber"> Number of the collided icosidodecahedron</param>
    /// <param name="faceNumber"> Number of the collided face </param>
	public void AddColision(int icosiNumber, int faceNumber){
        bool isInserted=false;
		//add face and exit, if icosidodecahedron not exist add new one
		foreach (IcosiColision elem in _icosiCollison){
			if(elem.GetNumIcosi()==icosiNumber){
				elem.AddFace(faceNumber);
                isInserted = true;
				break;
			}
		}
		if(!isInserted) 
            _icosiCollison.Add(new IcosiColision(icosiNumber,faceNumber));
	}  

    /// <summary>
    /// Take the number of the first collided icosidodecahedron
    /// </summary>
    /// <returns> Return the int number of an icosidodecahedron</returns>
	public int GetFirstNumIcosi(){
		if (_icosiCollison.Count!=0) 
            return _icosiCollison[0].GetNumIcosi();
		else 
            return -1;
	}

    /// <summary>
    /// Take the collided faces from first icosidodecahedron and remove this icosidodecahedron
    /// </summary>
    /// <returns> Return int list with the number of the faces collided or Null if list is empty</returns>
	public List<int> GetNextColisionFaces(){
		if (_icosiCollison.Count!=0){
			IcosiColision temporal = _icosiCollison[0];
			_icosiCollison.RemoveAt(0);
			return temporal.GetFaces();
		}else return null;
	}

    /// <summary>
    /// Check list of icosidodecahedron is empty  
    /// </summary>
    /// <returns> Return true if list is empty or false if is not</returns>
	public bool IsEmpty(){ 
		if(_icosiCollison.Count!=0) 
            return false;
		else 
            return true;
	}


    public int Count() {
        return _icosiCollison.Count;
    }
}