﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class responsible for make a visual alpha transition to platform.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class Transition : MonoBehaviour {

    private float _cont;
    private Color _color;
    private List<string> _shaders;
    private int _time;
 
	// Use this for initialization
	void Awake () {
        _time = 40;
        _cont = 0.0f;
        foreach (Transform elem in transform)
            foreach (Material mat in elem.gameObject.renderer.materials) {
                mat.shader = Shader.Find("Transparent/Diffuse");
                _color = mat.color;
                _color.a = _cont;
                mat.color = _color;
            }
	}
	
	// Update is called once per frame
	void Update () {
        if (_cont < 1.0f) {
            _cont += (float)(1.0f / _time);
            foreach (Transform elem in transform)
                foreach (Material mat in elem.gameObject.renderer.materials) {
                    _color = mat.color;
                    _color.a = _cont;
                    mat.color = _color;
                }
        } else {
            foreach (Transform elem in transform)
                foreach (Material mat in elem.gameObject.renderer.materials)
                    mat.shader = Shader.Find("Bumped Specular");
            Destroy(this.gameObject.GetComponent<Transition>());
            Destroy(this.gameObject.GetComponent<Transition>());
        }
	    
	}
}
