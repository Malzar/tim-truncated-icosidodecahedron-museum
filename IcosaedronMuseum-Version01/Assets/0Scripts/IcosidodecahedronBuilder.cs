﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class controls an icosidodecahedron gameobject.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class IcosidodecahedronBuilder {

    private float a, b, c, d;
    private GameObject _icosidodecahedronObject;
    private int _gameObjectNumber;
    private int _faceMode;
    private int _fatherNumber;

    /// <summary>
    /// Builder from icosidodecahedron gameobject
    /// </summary>
    /// <param name="pos">Position of center to icosidodecahedron</param>
    /// <param name="gameObjectNumber">Number of icosidodecahedron</param>
    /// <param name="faceMode">Platform model</param>
    /// <param name="fatherNumber">Number of icosidodecahedron father</param>
    public IcosidodecahedronBuilder(Vector3 pos, int gameObjectNumber, int faceMode, int fatherNumber) {
        a = 0.0f;
        b = 8.523893f;
        c = 22.31584f;
        d = 13.79195f;
        _icosidodecahedronObject = new GameObject();
        _icosidodecahedronObject.tag = "TI";
        _icosidodecahedronObject.transform.position = pos;
        _gameObjectNumber = gameObjectNumber;
        _icosidodecahedronObject.name = "Icosidodecahedron_" + _gameObjectNumber.ToString();
        _faceMode = faceMode;
        _fatherNumber = fatherNumber;
    }

    /// <summary>
    /// Destroyer
    /// </summary>
    public void Destroy() {
        GameObject.DestroyImmediate(_icosidodecahedronObject);
    }

    /// <summary>
    /// Destroy all platform from icosidodecahedron
    /// </summary>
    public void DestroyFaces() {
        foreach (Transform child in _icosidodecahedronObject.transform)
            GameObject.Destroy(child.gameObject);
    }

    /// <summary>
    /// Create a new game object component
    /// </summary>
    /// <param name="pos">Position of center to icosidodecahedron</param>
    public void ReCreate(Vector3 pos) {
        _icosidodecahedronObject = new GameObject();
        _icosidodecahedronObject.transform.position = pos;
        _icosidodecahedronObject.name = "Icosidodecahedron_" + _gameObjectNumber.ToString();
    }

    /// <summary>
    /// Instantiate a new face in icosidodecahedron
    /// </summary>
    /// <param name="numface">Face number to instantiate</param>
    /// <param name="withGhostLayer">Habilitate the Gosht layer componet</param>
    public void AddFace(int numface, bool withGhostLayer) {

        GameObject platform;

        if (_faceMode < 0 || _faceMode > 1) {
            Debug.LogError("AddFace: This mode not exist");
            return;
        }

        if (_faceMode == 0) {
            platform = GameObject.Instantiate(Resources.Load("PI")) as GameObject;
            platform.name = "PI_" + _gameObjectNumber.ToString() + "_" + numface.ToString();
        } else {
            platform = GameObject.Instantiate(Resources.Load("PE")) as GameObject;
            platform.name = "PE_" + _gameObjectNumber.ToString() + "_" + numface.ToString();
        }

        //add ghostLayer and component in all children
        if (withGhostLayer) {
            platform.AddComponent<GhostLayer>();
            platform.GetComponent<GhostLayer>().SetFatherNumber(_fatherNumber);
            platform.GetComponent<GhostLayer>().SetFaceNumber(numface);
            platform.GetComponent<GhostLayer>().SetNumberIcosi(_gameObjectNumber);
            platform.AddComponent<Rigidbody>();
            platform.collider.isTrigger = true;
            platform.rigidbody.useGravity = false;
        } else {
            foreach (Transform elem in platform.transform)
                elem.GetComponent<MeshRenderer>().enabled = true;
        }

        platform.transform.parent = _icosidodecahedronObject.transform;

        switch (numface) {
            case 1:
                platform.transform.localPosition = new Vector3(a, b, c);
                platform.transform.localEulerAngles = new Vector3(339.0948f, 0.0f, 0.0f);
                break;
            case 2:
                platform.transform.localPosition = new Vector3(-d, d, d);
                platform.transform.localEulerAngles = new Vector3(324.7356f, 315.0f, 322.2388f);
                break;
            case 3:
                platform.transform.localPosition = new Vector3(a, -b, c);
                platform.transform.localEulerAngles = new Vector3(339.0948f, 180.0f, 0.0f);
                platform.transform.localScale = platform.transform.localScale * -1;
                break;
            case 4:
                platform.transform.localPosition = new Vector3(d, d, d);
                platform.transform.localEulerAngles = new Vector3(324.7356f, 45.0f, 37.76124f);
                break;
            case 5:
                platform.transform.localPosition = new Vector3(-b, c, a);
                platform.transform.localEulerAngles = new Vector3(290.9052f, 270.0f, 300.0f);
                break;
            case 6:
                platform.transform.localPosition = new Vector3(-c, a, b);
                platform.transform.localEulerAngles = new Vector3(0.0f, 110.9052f, 30.0f);
                platform.transform.localScale = platform.transform.localScale * -1;
                break;
            case 7:
                platform.transform.localPosition = new Vector3(-d, -d, d);
                platform.transform.localEulerAngles = new Vector3(35.26439f, 315.0f, 337.7613f);
                break;
            case 8:
                platform.transform.localPosition = new Vector3(d, -d, d);
                platform.transform.localEulerAngles = new Vector3(35.26439f, 45f, 22.23876f);
                break;
            case 9:
                platform.transform.localPosition = new Vector3(c, a, b);
                platform.transform.localEulerAngles = new Vector3(0.0f, 249.0948f, 330.0f);
                platform.transform.localScale = platform.transform.localScale * -1;
                break;
            case 10:
                platform.transform.localPosition = new Vector3(b, c, a);
                platform.transform.localEulerAngles = new Vector3(290.9052f, 90.0f, 60.0f);
                break;
            case 11:
                platform.transform.localPosition = new Vector3(-d, d, -d);
                platform.transform.localEulerAngles = new Vector3(35.26439f, 45f, 22.23876f);
                platform.transform.localScale = platform.transform.localScale * -1;
                break;
            case 12:
                platform.transform.localPosition = new Vector3(-c, a, -b);
                platform.transform.localEulerAngles = new Vector3(0.0f, 249.0948f, 330.0f);
                break;
            case 13:
                platform.transform.localPosition = new Vector3(-b, -c, a);
                platform.transform.localEulerAngles = new Vector3(290.9052f, 90.0f, 60.0f);
                platform.transform.localScale = platform.transform.localScale * -1;
                break;
            case 14:
                platform.transform.localPosition = new Vector3(b, -c, a);
                platform.transform.localEulerAngles = new Vector3(290.9052f, 270.0f, 300.0f);
                platform.transform.localScale = platform.transform.localScale * -1;
                break;
            case 15:
                platform.transform.localPosition = new Vector3(c, a, -b);
                platform.transform.localEulerAngles = new Vector3(0.0f, 110.9052f, 30.0f);
                break;
            case 16:
                platform.transform.localPosition = new Vector3(d, d, -d);
                platform.transform.localEulerAngles = new Vector3(35.26439f, 315.0f, 337.7613f);
                platform.transform.localScale = platform.transform.localScale * -1;
                break;
            case 17:
                platform.transform.localPosition = new Vector3(a, b, -c);
                platform.transform.localEulerAngles = new Vector3(339.0948f, 180.0f, 0.0f);
                break;
            case 18:
                platform.transform.localPosition = new Vector3(-d, -d, -d);
                platform.transform.localEulerAngles = new Vector3(324.7356f, 45.0f, 37.76124f);
                platform.transform.localScale = platform.transform.localScale * -1;
                break;
            case 19:
                platform.transform.localPosition = new Vector3(d, -d, -d);
                platform.transform.localEulerAngles = new Vector3(324.7356f, 315.0f, 322.2388f);
                platform.transform.localScale = platform.transform.localScale * -1;
                break;
            case 20:
                platform.transform.localPosition = new Vector3(a, -b, -c);
                platform.transform.localEulerAngles = new Vector3(339.0948f, 0.0f, 0.0f);
                platform.transform.localScale = platform.transform.localScale * -1;
                break;
            default:
                Debug.LogError("AddFace: This face not exist");
                break;
        }
    }

    /// <summary>
    /// Create the center of new icosidodecahedron. 
    /// </summary>
    /// <param name="limitFace"> Is the face limit number used to connect with other icosidodecahedron</param>
    /// <param name="borderFace"> Is the face border number used to connect with other icosidodecahedron</param>
    /// <returns>Return Vector 3 with centre of new icosidodecahedron</returns>
    public Vector3 GetNewCenterPoint(int limitFace, int borderFace) {
        Vector3 middlePoint;
        Vector3 newPoint;
        GameObject platform;
        int numCorridor = 0;

        //give corridor
        switch (limitFace) {
            case 1:
                if (borderFace == 3) numCorridor = 1;
                if (borderFace == 2) numCorridor = 2;
                if (borderFace == 4) numCorridor = 3;
                break;
            case 2:
                if (borderFace == 6) numCorridor = 1;
                if (borderFace == 5) numCorridor = 2;
                if (borderFace == 1) numCorridor = 3;
                break;
            case 3:
                if (borderFace == 1) numCorridor = 1;
                if (borderFace == 7) numCorridor = 2;
                if (borderFace == 8) numCorridor = 3;
                break;
            case 4:
                if (borderFace == 9) numCorridor = 1;
                if (borderFace == 1) numCorridor = 2;
                if (borderFace == 10) numCorridor = 3;
                break;
            case 5:
                if (borderFace == 11) numCorridor = 1;
                if (borderFace == 10) numCorridor = 2;
                if (borderFace == 2) numCorridor = 3;
                break;
            case 6:
                if (borderFace == 2) numCorridor = 1;
                if (borderFace == 12) numCorridor = 2;
                if (borderFace == 7) numCorridor = 3;
                break;
            case 7:
                if (borderFace == 13) numCorridor = 1;
                if (borderFace == 6) numCorridor = 2;
                if (borderFace == 3) numCorridor = 3;
                break;
            case 8:
                if (borderFace == 14) numCorridor = 1;
                if (borderFace == 3) numCorridor = 2;
                if (borderFace == 9) numCorridor = 3;
                break;
            case 9:
                if (borderFace == 4) numCorridor = 1;
                if (borderFace == 8) numCorridor = 2;
                if (borderFace == 15) numCorridor = 3;
                break;
            case 10:
                if (borderFace == 16) numCorridor = 1;
                if (borderFace == 4) numCorridor = 2;
                if (borderFace == 5) numCorridor = 3;
                break;
            case 11:
                if (borderFace == 5) numCorridor = 1;
                if (borderFace == 17) numCorridor = 2;
                if (borderFace == 12) numCorridor = 3;
                break;
            case 12:
                if (borderFace == 18) numCorridor = 1;
                if (borderFace == 11) numCorridor = 2;
                if (borderFace == 6) numCorridor = 3;
                break;
            case 13:
                if (borderFace == 7) numCorridor = 1;
                if (borderFace == 18) numCorridor = 2;
                if (borderFace == 14) numCorridor = 3;
                break;
            case 14:
                if (borderFace == 8) numCorridor = 1;
                if (borderFace == 13) numCorridor = 2;
                if (borderFace == 19) numCorridor = 3;
                break;
            case 15:
                if (borderFace == 19) numCorridor = 1;
                if (borderFace == 9) numCorridor = 2;
                if (borderFace == 16) numCorridor = 3;
                break;
            case 16:
                if (borderFace == 10) numCorridor = 1;
                if (borderFace == 15) numCorridor = 2;
                if (borderFace == 17) numCorridor = 3;
                break;
            case 17:
                if (borderFace == 20) numCorridor = 1;
                if (borderFace == 16) numCorridor = 2;
                if (borderFace == 11) numCorridor = 3;
                break;
            case 18:
                if (borderFace == 12) numCorridor = 1;
                if (borderFace == 20) numCorridor = 2;
                if (borderFace == 13) numCorridor = 3;
                break;
            case 19:
                if (borderFace == 15) numCorridor = 1;
                if (borderFace == 14) numCorridor = 2;
                if (borderFace == 20) numCorridor = 3;
                break;
            case 20:
                if (borderFace == 17) numCorridor = 1;
                if (borderFace == 19) numCorridor = 2;
                if (borderFace == 18) numCorridor = 3;
                break;
            default:
                Debug.LogError("GetNewCenterPoint: the given face isn't correct");
                break;
        }
        if (numCorridor == 0) {
            Debug.LogError("GetNewCenterPoint: the given border face isn't correct");
        }

        if (_faceMode == 0) {
            platform = _icosidodecahedronObject.transform.FindChild("PI_" + _gameObjectNumber.ToString() + "_" + limitFace.ToString()).gameObject;
            middlePoint = platform.transform.FindChild("Platform-Inside-Corridor_" + numCorridor.ToString()).transform.position;
        } else {
            platform = _icosidodecahedronObject.transform.FindChild("PE_" + _gameObjectNumber.ToString() + "_" + limitFace.ToString()).gameObject;
            middlePoint = platform.transform.FindChild("Platform-Outside-Corridor_" + numCorridor.ToString()).transform.position;
        }

        newPoint.x = 2 * middlePoint.x - _icosidodecahedronObject.transform.position.x;
        newPoint.y = 2 * middlePoint.y - _icosidodecahedronObject.transform.position.y;
        newPoint.z = 2 * middlePoint.z - _icosidodecahedronObject.transform.position.z;
        return newPoint;
    }

    /// <summary>
    /// Get the number of gameobject in the scene.
    /// </summary>
    /// <returns>Return interger number of icosidodecahedron </returns>
    public int GetNumber() { return _gameObjectNumber; }

    /// <summary>
    ///  Get the mode of faces in the icosidodecahedron.
    /// </summary>
    /// <returns>return integer number of face mode</returns>
    public int GetFaceMode() { return _faceMode; }

    /// <summary>
    /// Get the number of icosidodecahedron father.
    /// </summary>
    /// <returns>Return interger number of icosidodecahedron father</returns>
    public int GetFatherNumber() { return _fatherNumber; }

    /// <summary>
    /// Get the connected corridor between two faces.
    /// </summary>
    /// <param name="limitFace">The number of limit face</param>
    /// <param name="borderFace">The number of border face</param>
    /// <returns></returns>
    public static int GetCorridorUnion(int limitFace, int borderFace) {
        int numCorridor = 0;

        switch (limitFace) {
            case 1:
                if (borderFace == 3) numCorridor = 1;
                if (borderFace == 2) numCorridor = 2;
                if (borderFace == 4) numCorridor = 3;
                break;
            case 2:
                if (borderFace == 6) numCorridor = 1;
                if (borderFace == 5) numCorridor = 2;
                if (borderFace == 1) numCorridor = 3;
                break;
            case 3:
                if (borderFace == 1) numCorridor = 1;
                if (borderFace == 7) numCorridor = 2;
                if (borderFace == 8) numCorridor = 3;
                break;
            case 4:
                if (borderFace == 9) numCorridor = 1;
                if (borderFace == 1) numCorridor = 2;
                if (borderFace == 10) numCorridor = 3;
                break;
            case 5:
                if (borderFace == 11) numCorridor = 1;
                if (borderFace == 10) numCorridor = 2;
                if (borderFace == 2) numCorridor = 3;
                break;
            case 6:
                if (borderFace == 2) numCorridor = 1;
                if (borderFace == 12) numCorridor = 2;
                if (borderFace == 7) numCorridor = 3;
                break;
            case 7:
                if (borderFace == 13) numCorridor = 1;
                if (borderFace == 6) numCorridor = 2;
                if (borderFace == 3) numCorridor = 3;
                break;
            case 8:
                if (borderFace == 14) numCorridor = 1;
                if (borderFace == 3) numCorridor = 2;
                if (borderFace == 9) numCorridor = 3;
                break;
            case 9:
                if (borderFace == 4) numCorridor = 1;
                if (borderFace == 8) numCorridor = 2;
                if (borderFace == 15) numCorridor = 3;
                break;
            case 10:
                if (borderFace == 16) numCorridor = 1;
                if (borderFace == 4) numCorridor = 2;
                if (borderFace == 5) numCorridor = 3;
                break;
            case 11:
                if (borderFace == 5) numCorridor = 1;
                if (borderFace == 17) numCorridor = 2;
                if (borderFace == 12) numCorridor = 3;
                break;
            case 12:
                if (borderFace == 18) numCorridor = 1;
                if (borderFace == 11) numCorridor = 2;
                if (borderFace == 6) numCorridor = 3;
                break;
            case 13:
                if (borderFace == 7) numCorridor = 1;
                if (borderFace == 18) numCorridor = 2;
                if (borderFace == 14) numCorridor = 3;
                break;
            case 14:
                if (borderFace == 8) numCorridor = 1;
                if (borderFace == 13) numCorridor = 2;
                if (borderFace == 19) numCorridor = 3;
                break;
            case 15:
                if (borderFace == 19) numCorridor = 1;
                if (borderFace == 9) numCorridor = 2;
                if (borderFace == 16) numCorridor = 3;
                break;
            case 16:
                if (borderFace == 10) numCorridor = 1;
                if (borderFace == 15) numCorridor = 2;
                if (borderFace == 17) numCorridor = 3;
                break;
            case 17:
                if (borderFace == 20) numCorridor = 1;
                if (borderFace == 16) numCorridor = 2;
                if (borderFace == 11) numCorridor = 3;
                break;
            case 18:
                if (borderFace == 12) numCorridor = 1;
                if (borderFace == 20) numCorridor = 2;
                if (borderFace == 13) numCorridor = 3;
                break;
            case 19:
                if (borderFace == 15) numCorridor = 1;
                if (borderFace == 14) numCorridor = 2;
                if (borderFace == 20) numCorridor = 3;
                break;
            case 20:
                if (borderFace == 17) numCorridor = 1;
                if (borderFace == 19) numCorridor = 2;
                if (borderFace == 18) numCorridor = 3;
                break;
            default:
                Debug.LogError("GetCorridor: the given face isn't correct");
                break;
        }

        return numCorridor;
    }

    /// <summary>
    /// Block a corridor from a face of a icosidodecahedron.
    /// </summary>
    /// <param name="faceNumber"> Number of the face.</param>
    /// <param name="numCorridor"> Number of the corridor to block.</param>
    public void BlockFace(int faceNumber, int numCorridor) {
        GameObject face;
        int otherCorridor = 0;

        if (_faceMode == 0) face = _icosidodecahedronObject.transform.FindChild("PI_" + _gameObjectNumber.ToString() + "_" + faceNumber.ToString()).gameObject;
        else face = _icosidodecahedronObject.transform.FindChild("PE_" + _gameObjectNumber.ToString() + "_" + faceNumber.ToString()).gameObject;

        foreach (Transform elem in face.transform) {
            string name = elem.name;
            string wall;
            name = name.Substring(name.IndexOf("-") + 1);
            name = name.Substring(name.IndexOf("-") + 1);
            wall = name.Substring(name.IndexOf("_") + 1);
            name = name.Remove(name.IndexOf("_"));
            if (name.Equals("Wall"))
                otherCorridor = int.Parse(wall);
        }
        if (otherCorridor == 0)
            RemoveCorridor(numCorridor, face);
        else {
            GameObject.DestroyImmediate(face);
            AddFace(faceNumber, false);

            if (_faceMode == 0) face = _icosidodecahedronObject.transform.FindChild("PI_" + _gameObjectNumber.ToString() + "_" + faceNumber.ToString()).gameObject;
            else face = _icosidodecahedronObject.transform.FindChild("PE_" + _gameObjectNumber.ToString() + "_" + faceNumber.ToString()).gameObject;

            GameObject.DestroyImmediate(face.GetComponent<GhostLayer>());
            BlockFace(faceNumber, numCorridor, otherCorridor);
        }
    }

    /// <summary>
    /// Block two corridors from a face of a icosidodecahedron.
    /// </summary>
    /// <param name="faceNumber"> Number of the face.</param>
    /// <param name="numCorriorA"> Number of the corridor to block.</param>
    /// <param name="numCorridorB"> Number of the corridor to block.</param>
    public void BlockFace(int faceNumber, int numCorriorA, int numCorridorB) {
        GameObject face;

        if (_faceMode == 0) face = _icosidodecahedronObject.transform.FindChild("PI_" + _gameObjectNumber.ToString() + "_" + faceNumber.ToString()).gameObject;
        else face = _icosidodecahedronObject.transform.FindChild("PE_" + _gameObjectNumber.ToString() + "_" + faceNumber.ToString()).gameObject;

        switch (numCorriorA + numCorridorB) {
            case 3:
                RemoveAllCorridorExcep(3, face);
                break;
            case 4:
                RemoveAllCorridorExcep(2, face);
                break;
            case 5:
                RemoveAllCorridorExcep(1, face);
                break;
            default:
                Debug.Log("BlockFace: corridors isn't correct" + _gameObjectNumber + " cooridor 1:" + numCorriorA + " 2:" + numCorridorB);
                break;
        }
    }

    /// <summary>
    /// Proces to remove a corridor from a face.
    /// </summary>
    /// <param name="corridorNumber">Number of the corridor to block</param>
    /// <param name="oldFace">Platform object to block</param>
    private void RemoveCorridor(int corridorNumber, GameObject oldFace) {
        GameObject wallPrefab;
        Transform newFace;
        Transform aux;

        if (_faceMode == 1) wallPrefab = GameObject.Instantiate(Resources.Load("PEW")) as GameObject;
        else wallPrefab = GameObject.Instantiate(Resources.Load("PIW")) as GameObject;

        if (oldFace.transform.localScale.x < 0) wallPrefab.transform.localScale = new Vector3(-1f, -1f, -1f);

        wallPrefab.transform.position = oldFace.transform.position;
        wallPrefab.transform.rotation = oldFace.transform.rotation;

        if (_faceMode == 1) newFace = wallPrefab.transform.FindChild("PEW1");
        else newFace = wallPrefab.transform.FindChild("PIW1");

        newFace.parent = null;
        newFace.name = oldFace.transform.name;
        if (_faceMode == 1) {
            switch (corridorNumber) {
                case 1:
                    newFace.rotation *= Quaternion.Euler(0f, 0f, 240f);
                    aux = wallPrefab.transform.FindChild("Platform-Outside-Wall_1");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Outside-Corridor_1").gameObject);
                    break;
                case 2:
                    newFace.rotation *= Quaternion.Euler(0f, 0f, 120f);
                    aux = wallPrefab.transform.FindChild("Platform-Outside-Wall_2");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Outside-Corridor_2").gameObject);
                    break;
                case 3:
                    aux = wallPrefab.transform.FindChild("Platform-Outside-Wall_3");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Outside-Corridor_3").gameObject);
                    break;
            }
        } else {
            switch (corridorNumber) {
                case 1:
                    newFace.rotation *= Quaternion.Euler(0f, 0f, 240f);
                    aux = wallPrefab.transform.FindChild("Platform-Inside-Wall_1");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Inside-Corridor_1").gameObject);
                    break;
                case 2:
                    newFace.rotation *= Quaternion.Euler(0f, 0f, 120f);
                    aux = wallPrefab.transform.FindChild("Platform-Inside-Wall_2");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Inside-Corridor_2").gameObject);
                    break;
                case 3:
                    aux = wallPrefab.transform.FindChild("Platform-Inside-Wall_3");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Inside-Corridor_3").gameObject);
                    break;
            }
        }

        for (int elem = 0; elem < oldFace.transform.childCount; elem++) {
            oldFace.transform.GetChild(elem).parent = newFace;
            elem--;
        }

        newFace.parent = oldFace.transform.parent;
        newFace.gameObject.AddComponent<Transition>();
        GameObject.Destroy(wallPrefab);
        GameObject.Destroy(oldFace);
    }

    /// <summary>
    /// Proces to remove two corridor from a face.
    /// </summary>
    /// <param name="corridorNumber">Number of the corridor not blocked</param>
    /// <param name="oldFace">Platform object to block</param>
    private void RemoveAllCorridorExcep(int corridorNumber, GameObject oldFace) {
        GameObject wallPrefab;
        Transform newFace;
        Transform aux;

        if (_faceMode == 1) wallPrefab = GameObject.Instantiate(Resources.Load("PEW")) as GameObject;
        else wallPrefab = GameObject.Instantiate(Resources.Load("PIW")) as GameObject;

        if (oldFace.transform.localScale.x < 0) wallPrefab.transform.localScale = new Vector3(-1f, -1f, -1f);

        wallPrefab.transform.position = oldFace.transform.position;
        wallPrefab.transform.rotation = oldFace.transform.rotation;

        if (_faceMode == 1) newFace = wallPrefab.transform.FindChild("PEW2");
        else newFace = wallPrefab.transform.FindChild("PIW2");

        newFace.parent = null;
        newFace.name = oldFace.transform.name;
        if (_faceMode == 1) {
            switch (corridorNumber) {
                case 1:
                    aux = wallPrefab.transform.FindChild("Platform-Outside-Wall_2");
                    aux.parent = newFace;
                    aux = wallPrefab.transform.FindChild("Platform-Outside-Wall_3");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Outside-Corridor_2").gameObject);
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Outside-Corridor_3").gameObject);
                    break;
                case 2:
                    newFace.rotation *= Quaternion.Euler(0f, 0f, 240f);
                    aux = wallPrefab.transform.FindChild("Platform-Outside-Wall_1");
                    aux.parent = newFace;
                    aux = wallPrefab.transform.FindChild("Platform-Outside-Wall_3");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Outside-Corridor_1").gameObject);
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Outside-Corridor_3").gameObject);
                    break;
                case 3:
                    newFace.rotation *= Quaternion.Euler(0f, 0f, 120f);
                    aux = wallPrefab.transform.FindChild("Platform-Outside-Wall_1");
                    aux.parent = newFace;
                    aux = wallPrefab.transform.FindChild("Platform-Outside-Wall_2");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Outside-Corridor_1").gameObject);
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Outside-Corridor_2").gameObject);
                    break;
            }
        } else {
            switch (corridorNumber) {
                case 1:
                    aux = wallPrefab.transform.FindChild("Platform-Inside-Wall_2");
                    aux.parent = newFace;
                    aux = wallPrefab.transform.FindChild("Platform-Inside-Wall_3");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Inside-Corridor_2").gameObject);
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Inside-Corridor_3").gameObject);
                    break;
                case 2:
                    newFace.rotation *= Quaternion.Euler(0f, 0f, 240f);
                    aux = wallPrefab.transform.FindChild("Platform-Inside-Wall_1");
                    aux.parent = newFace;
                    aux = wallPrefab.transform.FindChild("Platform-Inside-Wall_3");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Inside-Corridor_1").gameObject);
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Inside-Corridor_3").gameObject);
                    break;
                case 3:
                    newFace.rotation *= Quaternion.Euler(0f, 0f, 120f);
                    aux = wallPrefab.transform.FindChild("Platform-Inside-Wall_1");
                    aux.parent = newFace;
                    aux = wallPrefab.transform.FindChild("Platform-Inside-Wall_2");
                    aux.parent = newFace;
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Inside-Corridor_1").gameObject);
                    GameObject.Destroy(oldFace.transform.FindChild("Platform-Inside-Corridor_2").gameObject);
                    break;
            }
        }
        for (int elem = 0; elem < oldFace.transform.childCount; elem++) {
            oldFace.transform.GetChild(elem).parent = newFace;
            elem--;
        }

        newFace.parent = oldFace.transform.parent;
        newFace.gameObject.AddComponent<Transition>();
        GameObject.Destroy(wallPrefab);
        GameObject.Destroy(oldFace);
    }

    /// <summary>
    /// Activation of render from all platform
    /// </summary>
    public void Rendered() {
        foreach (Transform platform in _icosidodecahedronObject.transform) {
            foreach (Transform elem in platform.transform) {
                elem.GetComponent<MeshRenderer>().enabled = true;
            }
            platform.gameObject.AddComponent<Transition>();
        }
    }

    /// <summary>
    /// Get a platforrm
    /// </summary>
    /// <param name="face">Number of the platform</param>
    /// <returns>Return a gameobject platform with face number</returns>
    public Transform GetFace(int face) {
        if (_faceMode == 0)
            return _icosidodecahedronObject.transform.FindChild("PI_" + _gameObjectNumber.ToString() + "_" + face);
        else
            return _icosidodecahedronObject.transform.FindChild("PE_" + _gameObjectNumber.ToString() + "_" + face);
    }

    /// <summary>
    /// Activate process performance against collision
    /// </summary>
    public void Checkfaces() {
        foreach (Transform elem in _icosidodecahedronObject.transform)
            if (elem.GetComponent<GhostLayer>() != null)
                elem.GetComponent<GhostLayer>().Check();
    }
}
