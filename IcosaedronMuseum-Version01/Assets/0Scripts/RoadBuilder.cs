﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class control the proces to create icosidodecahedron and connected.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class RoadBuilder {

    private IcosidodecahedronList _availablesIcosi;
    private IcosidodecahedronList _fullIcosi;
    private IcosidodecahedronList _icosiToCheck;
    private IcosidodecahedronList _borderList;
    private ColisionList _colisionList;
    private SpaceDivision _spaceDivision;
    private List<int> _freeNumbers;
    private List<int> _icosiInScene;
    private List<TupleList> _icosiToBlock;
    private int _lastIcosidodecahedron;
    private Vector3 _center;
    private float _halfSize;
    private int _seed;
    private int _facesput;

    /////////////////////////////////////////
    //To rebuild
    public int TIREBUILD;
    public int TIWITHMOREFACES;
    //To generate
    public int TIFATHER;
    public int TICHILD;
    //To repair
    public int TIREPAIR;
    public int TIFACESREP;
    //To check
    public int TICHECK;
    public int TICHEMOREFACE;
    //To move
    public int TISAVED;
    public int TILOADED;
    /////////////////////////////////////////

    /// <summary>
    /// Builder
    /// </summary>
    /// <param name="halfSize">Half size of view distance.</param>
    /// <param name="center">Position to begin the procces.</param>
    /// <param name="seed">Seed number to add to random.</param>
    public RoadBuilder(float halfSize, Vector3 center, int seed) {
        _availablesIcosi = new IcosidodecahedronList();
        _fullIcosi = new IcosidodecahedronList();
        _icosiToCheck = new IcosidodecahedronList();
        _borderList = new IcosidodecahedronList();
        _colisionList = new ColisionList();
        _freeNumbers = new List<int>();
        _icosiInScene = new List<int>();
        _icosiToBlock = new List<TupleList>();
        _halfSize = halfSize;
        _center = center;
        _spaceDivision = new SpaceDivision(_halfSize, _center);
        _lastIcosidodecahedron = 1;
        _seed = seed;
    }

    /// <summary>
    /// Start the procces with first icosidodecahedron.
    /// </summary>
    /// <param name="faceMode">Number of the platform mode.</param>
    /// <param name="numFaces">Number of the face to use.</param>
    /// <param name="modeComposition">Number of the positioning mode face.</param>
    /// <param name="putAllFaces">The choice to put or not all faces.</param>
    public void Start(int faceMode, int numFaces, int modeComposition, bool putAllFaces) {
        System.Random rnd;
        if (_seed != 0)
            rnd = new System.Random((int)_seed);
        else
            rnd = new System.Random((int)Time.deltaTime);

        int inceptionFace = rnd.Next(1, 21);
        Icosidodecahedron icosi = new Icosidodecahedron(_center, 1, faceMode, inceptionFace, modeComposition, _seed, putAllFaces);
        _availablesIcosi.Add(icosi);
        _spaceDivision.Add(1, _center);
        icosi.PutInToScene(numFaces);
        _lastIcosidodecahedron++;
        icosi.Render();
        icosi.CheckFaces();
    }

    /// <summary>
    /// Instantiate player into scene.
    /// </summary>
    /// <returns>Return a player gameObject.</returns>
    public GameObject PutPJ() {
        Transform platforminception = _fullIcosi.GetByPos(0).GetRandomFace();
        Vector3 dir = (platforminception.parent.position - platforminception.position).normalized;

        dir *= 3f;
        GameObject PJ = GameObject.Instantiate(Resources.Load("PJ")) as GameObject;

        if(platforminception.name.Contains("PE")) 
            PJ.transform.position = platforminception.position - dir;
        else
            PJ.transform.position = platforminception.position + dir;

        platforminception.gameObject.AddComponent<GravityAttractor>();
        PJ.AddComponent<GravityBody>();
        PJ.AddComponent<FirstPersonController>();
        PJ.GetComponent<FirstPersonController>().SetPlatform(platforminception.gameObject);
        PJ.transform.FindChild("ColliderControler").GetComponent<ColliderController>().SetInitialPlatformPlatform(platforminception.gameObject);
        return PJ;
    }

    /// <summary>
    /// Instantiate player into a specific icosidodecahedron
    /// </summary>
    /// <param name="IcosiNumber">Number of the icosidodecahedron</param>
    /// <returns>Return a player gameObject.</returns>
    public GameObject PutPJ(int IcosiNumber) {
        Transform platforminception;
        Icosidodecahedron actualIcosi = null;
        if (IcosiNumber < 0) {
            if(_fullIcosi.Count()>0)
                actualIcosi = _fullIcosi.GetByPos(Random.Range(0, (_fullIcosi.Count() - 1)));
        } else {
            actualIcosi = _fullIcosi.GetByNumber(IcosiNumber);
            if (actualIcosi == null)
                actualIcosi = _borderList.GetByNumber(IcosiNumber);
        }
        if (actualIcosi != null) {
            platforminception = actualIcosi.GetRandomFace();

            Vector3 dir = (platforminception.parent.position - platforminception.position).normalized;

            dir *= 3f;
            GameObject PJ = GameObject.Instantiate(Resources.Load("PJ")) as GameObject;

            if (platforminception.name.Contains("PE"))
                PJ.transform.position = platforminception.position - dir;
            else
                PJ.transform.position = platforminception.position + dir;

            platforminception.gameObject.AddComponent<GravityAttractor>();
            PJ.AddComponent<GravityBody>();
            PJ.AddComponent<FirstPersonController>();
            PJ.GetComponent<FirstPersonController>().SetPlatform(platforminception.gameObject);
            PJ.transform.FindChild("ColliderControler").GetComponent<ColliderController>().SetInitialPlatformPlatform(platforminception.gameObject);

            return PJ;
        } else {
            return null;
        }
    }

    /// <summary>
    /// Generate all possible icosiododecahedron sons.
    /// </summary>
    /// <param name="numberOfFaces">Number of the face to use.</param>
    /// <param name="modeComposition">Number of the positioning mode face.</param>
    /// <param name="putAllFaces">The choice to put or not all faces.</param>
    /// <returns>Return true if can generate new icosidodecahedron, false in other case.</returns>
    public bool GenerateSons(int numberOfFaces, int modeComposition, bool putAllFaces) {
        Vector3 newCenter;
        int faceMode, borderFace, limitFace, fatherNumber;
        List<IcosidodecahedronUnion> unions;
        Icosidodecahedron actualIcosi, newIcosidodecahedron;
        bool canGenerated = false;
        /////////////////////////
        TIFATHER = _availablesIcosi.Count();
        TICHILD = 0;
        /////////////////////////
        while (!_availablesIcosi.IsEmpty()) {
            //get the firt empty icosidodecahedron to add other icosidodecahedron
            actualIcosi = _availablesIcosi.GetNext();
            if (actualIcosi.GetUnion().Count == 0)
                actualIcosi.GenerateUnions();
            unions = actualIcosi.GetUnion();
            //Switching between modes and add new number to icosidodecahedron
            faceMode = Mathf.Abs(unions[0].GetInceptionMode() - 1);
            fatherNumber = actualIcosi.GetNumber();
            _freeNumbers.Sort();
            //generate all new icosidodecahedron from his union
            for (int i = 0; i < unions.Count; i++) {
                newCenter = unions[i].GetCenter();
                if (IsInside(newCenter)) {
                    /////////////////////////
                    TICHILD++;
                    /////////////////////////
                    limitFace = unions[i].GetLimitFace();
                    borderFace = unions[i].GetBorderFace();
                    if (_freeNumbers.Count == 0) {
                        //Debug.Log("Hello icosi " + _lastIcosidodecahedron);
                        newIcosidodecahedron = new Icosidodecahedron(newCenter, _lastIcosidodecahedron, faceMode, InternalRoadGenerator.GetOppositeFace(limitFace), InternalRoadGenerator.GetOppositeFace(borderFace), fatherNumber, modeComposition, _seed, putAllFaces);
                        _lastIcosidodecahedron++;
                    } else {
                        //Debug.Log("Hello icosi " + _freeNumbers[0]);
                        newIcosidodecahedron = new Icosidodecahedron(newCenter, _freeNumbers[0], faceMode, InternalRoadGenerator.GetOppositeFace(limitFace), InternalRoadGenerator.GetOppositeFace(borderFace), fatherNumber, modeComposition, _seed, putAllFaces);
                        _freeNumbers.RemoveAt(0);
                    }
                    newIcosidodecahedron.SetDataFather(limitFace, IcosidodecahedronBuilder.GetCorridorUnion(limitFace, borderFace));
                    newIcosidodecahedron.PutInToScene(numberOfFaces);
                    _icosiToCheck.Add(newIcosidodecahedron);
                    canGenerated = true;
                    unions.RemoveAt(i);
                    i--;
                }
            }
            if (unions.Count == 0) {
                _fullIcosi.Add(actualIcosi);
                //add into teleport list
                _icosiInScene.Add(actualIcosi.GetNumber());
            } else {
                _borderList.Add(actualIcosi);
            }
        }
        return canGenerated;
    }

    /// <summary>
    /// Repair the corridors which cannot be used.
    /// </summary>
    public void Repair() {
        Icosidodecahedron actualIcosi;
        ////////////////////
        TIREPAIR = _icosiToBlock.Count;
        TIFACESREP = 0;
        ////////////////////
        foreach (TupleList elem in _icosiToBlock) {
            actualIcosi = _fullIcosi.GetIcosi(elem.GetIcosiNumber());
            if (actualIcosi != null) {
                /////////////////////////////
                TIFACESREP += elem.GetFaces().Count;
                ////////////////////////////
                foreach (Tuple tuple in elem.GetFaces()) {
                    if (tuple.GetThird() < 0) 
                        actualIcosi.BlockFace(tuple.GetFirst(), tuple.GetSecond());
                    else 
                        actualIcosi.BlockFace(tuple.GetFirst(), tuple.GetSecond(), tuple.GetThird());
                }
                _fullIcosi.Add(actualIcosi);
            } else {
                /////////////////////////////
                TIFACESREP += elem.GetFaces().Count;
                ////////////////////////////
                actualIcosi = _borderList.GetIcosi(elem.GetIcosiNumber());
                if (actualIcosi != null) {
                    foreach (Tuple tuple in elem.GetFaces()) {
                        if (tuple.GetThird() < 0) 
                            actualIcosi.BlockFace(tuple.GetFirst(), tuple.GetSecond());
                        else 
                            actualIcosi.BlockFace(tuple.GetFirst(), tuple.GetSecond(), tuple.GetThird());
                    }
                    _borderList.Add(actualIcosi);
                }
            }
        }
        _icosiToBlock.Clear();
    }

    /// <summary>
    /// Rebuild collaided icosidodecahedrons.
    /// </summary>
    /// <returns>Return true if have more icosidodecahedrons to check collision, false in other case.</returns>
    public bool Rebuild() {
        List<int> facesCollided;
        Icosidodecahedron actualIcosi;
        IcosidodecahedronList temporalList = new IcosidodecahedronList();
        /////////////////////////
        TIREBUILD = _colisionList.Count();
        TIWITHMOREFACES = 0;
        /////////////////////////
        //try to rebuild a icosidodecahedron collided
        while (!_colisionList.IsEmpty()) {
            //get the first collided icosidodecahedron to rebuild
            actualIcosi = _icosiToCheck.GetIcosi(_colisionList.GetFirstNumIcosi());            
            /////////////////////////
            if (actualIcosi._numFaces > 1)
                TIWITHMOREFACES++;
            /////////////////////////
            facesCollided = _colisionList.GetNextColisionFaces();
            if (actualIcosi.Rebuild(facesCollided)) {
                //Add rebuilt icosidodecahedron to temporal list for later checking collision
                temporalList.Add(actualIcosi);
            } else {
                //if icosidodecahedron can't rebuild, block corridor
                TupleList dataBlock;
                if (_icosiToBlock.Contains(new TupleList(actualIcosi.GetFatherNumber()))) {
                    dataBlock = _icosiToBlock.Find(x => x.GetIcosiNumber() == actualIcosi.GetFatherNumber());
                    dataBlock.Add(actualIcosi.GetFaceLimitFather(), actualIcosi.GetCorridorFather());
                } else {
                    //if icosidodecahedron isn't in blocked list, add it.
                    dataBlock = new TupleList(actualIcosi.GetFatherNumber());
                    dataBlock.Add(actualIcosi.GetFaceLimitFather(), actualIcosi.GetCorridorFather());
                    _icosiToBlock.Add(dataBlock);
                }
                //destroy actual icosidodecahedron
                _freeNumbers.Add(actualIcosi.GetNumber());
                actualIcosi.Destroy();
            }
        }

        //put icosidodecahedron cheched into availabe list
        while (!_icosiToCheck.IsEmpty()) {
            Icosidodecahedron icosiToSave = _icosiToCheck.GetNext();
            _spaceDivision.Add(icosiToSave.GetNumber(), icosiToSave.GetCenter());
            icosiToSave.Render();
            _availablesIcosi.Add(icosiToSave);
        }

        //put rebuilded icosidodecahedron into check list
        while (!temporalList.IsEmpty()) 
            _icosiToCheck.Add(temporalList.GetNext());

        return _icosiToCheck.IsEmpty();
    }

    /// <summary>
    /// Activate the proces to check collision to new icosidodecahedrons.
    /// </summary>
    public void Check() {
        TICHECK = _icosiToCheck.Count();
        TICHEMOREFACE = 0;
        for (int i = 0; i < _icosiToCheck.Count(); i++) {
            ///////////////////////////////////
            if (_icosiToCheck.GetByPos(i)._numFaces > 1)
                TICHEMOREFACE++;
            //////////////////////////////////
            _icosiToCheck.GetByPos(i).CheckFaces();
        }
    }

    /// <summary>
    /// Add collsion to the system
    /// </summary>
    /// <param name="icosahedronNumber">Number of the collided icosidodecahedron.</param>
    /// <param name="faceNumber">Number of the collided face.</param>
    public void AddColision(int icosahedronNumber, int faceNumber) {
        _colisionList.AddColision(icosahedronNumber, faceNumber);
    }

    /// <summary>
    /// Move the position of the center of render space.
    /// </summary>
    /// <param name="newCenter">The center of render space.</param>
    public void MoveSpaceCenter(Vector3 newCenter) {
        List<int> icosiToRemove;
        Icosidodecahedron actualIcosi;
        IcosidodecahedronList icosiListToExange = new IcosidodecahedronList();

        icosiToRemove = _spaceDivision.MoveCenter(newCenter);
        ////////////////////
        TISAVED = icosiToRemove.Count;
        ////////////////////
        foreach (int icosiPos in icosiToRemove) {
            actualIcosi = _fullIcosi.GetIcosi(icosiPos);
            if (actualIcosi == null)
                actualIcosi = _borderList.GetIcosi(icosiPos);

            _icosiInScene.Remove(actualIcosi.GetNumber());
            actualIcosi.Hide();
            icosiListToExange.Add(actualIcosi);
        }

        _spaceDivision.SaveIcosi(icosiListToExange, newCenter);
        icosiListToExange = _spaceDivision.GetIcosis(newCenter);

        _center = newCenter;
        ///////////////////
        TILOADED = icosiListToExange.Count();
        ///////////////////
        while (!icosiListToExange.IsEmpty()) {
            actualIcosi = icosiListToExange.GetNext();

            if (actualIcosi.GetUnion().Count == 0) {
                _fullIcosi.Add(actualIcosi);
                _icosiInScene.Add(actualIcosi.GetNumber());
            } else
                _availablesIcosi.Add(actualIcosi);

            actualIcosi.PutInToScene();
            actualIcosi.Render();
            _spaceDivision.Add(actualIcosi.GetNumber(), actualIcosi.GetCenter());
        }

        while (!_borderList.IsEmpty()) {
            _availablesIcosi.Add(_borderList.GetNext());
        }
    }

    /// <summary>
    /// Get all icosidodechaedron into the render space.
    /// </summary>
    /// <returns>Return a list with all icosidodecahedron into render space.</returns>
    public List<int> GetIcosiInScene() {
        _icosiInScene.Sort();
        return _icosiInScene; 
    }

    /// <summary>
    /// Check if have more icosidodecahedron to check.
    /// </summary>
    /// <returns>Return true if have more icosidodecahedron to check, false in other case.</returns>
    public bool MoreToRebuild() { return !_icosiToCheck.IsEmpty(); }

    /// <summary>
    /// Check if player is inside the area loaded.
    /// </summary>
    /// <param name="pos">Position of the player.</param>
    /// <returns>Return true if player is inside, false in other case.</returns>
    private bool IsInside(Vector3 pos) {
        float tamLimit = _halfSize - 25;
        float error = 0.0001f;

        bool x = (((pos.x + error) <= (_center.x + tamLimit) || (pos.x - error) <= (_center.x + tamLimit)) && ((pos.x + error) > (_center.x - tamLimit) || (pos.x - error) > (_center.x - tamLimit)));
        bool y = (((pos.y + error) <= (_center.y + tamLimit) || (pos.y - error) <= (_center.y + tamLimit)) && ((pos.y + error) > (_center.y - tamLimit) || (pos.y - error) > (_center.y - tamLimit)));
        bool z = (((pos.z + error) <= (_center.z + tamLimit) || (pos.z - error) <= (_center.z + tamLimit)) && ((pos.z + error) > (_center.z - tamLimit) || (pos.z - error) > (_center.z - tamLimit)));
        return x && y && z;
    }
}
