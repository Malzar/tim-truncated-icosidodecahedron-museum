﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : MonoBehaviour {

    float mouseSpeed = 5;
    float fingerSpeed = 0.1f;
    float maximumAngle = 60;
    float minimumAngle = -60;
    bool activateDamping = true;
    float dampingTime = 0.25f;

    private float currentX;
    private float currentY;

    private float targetX;
    private float targetY;

    private float xVel;
    private float yVel;

    private bool isMobile = false;

    private bool _wait;

    void Start() {
        ResetAngles();
        _wait = false;
        isMobile = (Application.platform == RuntimePlatform.IPhonePlayer) || (Application.platform == RuntimePlatform.Android);
    }

    void ResetAngles() {
        Vector3 angles = transform.localEulerAngles;
        currentX = angles.x;
        if (currentX > 90) currentX -= 360;
        if (currentX < -90) currentX += 360;
        targetX = currentX;

        currentY = targetY = angles.y;


    }

    void Update() {
        if (!_wait) {
            if (isMobile) {
                TouchOrbit();
            } else {
                MouseOrbit();
            }

            targetX = Mathf.Clamp(targetX, minimumAngle, maximumAngle);

            if (activateDamping) {
                currentX = Mathf.SmoothDampAngle(currentX, targetX, ref xVel, dampingTime);
                currentY = Mathf.SmoothDampAngle(currentY, targetY, ref yVel, dampingTime);
            } else {
                currentX = targetX;
                currentY = targetY;
            }

            transform.localRotation = Quaternion.Euler(currentX, currentY, 0);
        }
    }

    void MouseOrbit() {
        if (Input.GetMouseButton(0)) {
            targetX -= Input.GetAxis("Mouse Y") * mouseSpeed;
            targetY += Input.GetAxis("Mouse X") * mouseSpeed;
        }
    }

    void TouchOrbit() {
        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved) {
            // Get movement of the finger since last frame
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

            // Move object across XY plane
            //transform.Translate (-touchDeltaPosition.x * fingerSpeed, -touchDeltaPosition.y * fingerSpeed, 0);
            targetY += touchDeltaPosition.x * fingerSpeed;
            targetX -= touchDeltaPosition.y * fingerSpeed;
        }
    }

    public void Stop() { _wait = true; }

    public void Play() { _wait = false; }
}