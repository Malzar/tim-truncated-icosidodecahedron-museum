﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class is added to face's gameobject for the purpose of evaluate an response to collision with other game object.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class GhostLayer : MonoBehaviour {

    private bool _isDestroy;
    private List<string> _elementColision;
    private int _icosiNumber;
    private int _icosiFather;
    private int _faceNumber;
    private bool _wait;

    /// <summary>
    /// Unity function. Initialize parameters
    /// </summary>
    void Awake() {
        //Debug.Log("Hello " + this.name);
        _elementColision = new List<string>();
        _isDestroy = false;
        _wait = true;
    }

    /// <summary>
    /// Unity function. Main loop.
    /// </summary>
    void Update() {
        if (!_wait) {
            if (_elementColision.Count != 0) {
                foreach (string value in _elementColision) {
                    //if older element exist, delete this element
                    if (GameObject.Find(value) != null) {
                        _isDestroy = true;
                        break;
                    }
                }
            }
            if (_isDestroy) {
                //invoke event destroy
                GameObject.Find("Core").GetComponent<Core>().AddColision(_icosiNumber, _faceNumber);
                DestroyImmediate(this.gameObject);
            } else {
                //delete script, rigidbody and disable collider to avoid collision detection
                this.collider.isTrigger = false;
                Destroy(this.rigidbody);
                Destroy(this.GetComponent<GhostLayer>());
            }
        }
    }

    /// <summary>
    /// Check the collision with other game objects.
    /// </summary>
    /// <param name="other"> Game object collided</param>
    void OnTriggerEnter(Collider other) {
        //chek if other face is put before and isn't the father  
        if (other.GetComponent<GhostLayer>() == null) {
            if (other.tag == "Platform") {
                if (GetOtherNumber(other.name) != _icosiFather)
                    _isDestroy = true;
            } else
                _isDestroy = true;
        } else {
            if (_icosiNumber > other.GetComponent<GhostLayer>().GetNumberIcosi())
                //add the name of the collider to check later
                if (!_elementColision.Contains(other.name))
                    _elementColision.Add(other.name);
        }
    }

    /// <summary>
    /// Get the number of other icosidodecahedron's game object by name.
    /// </summary>
    /// <param name="name">String name to evaluate</param>
    /// <returns>Integer number of icosidodecahedron game object</returns>
    private int GetOtherNumber(string name) {
        name = name.Substring(name.IndexOf('_') + 1);
        name = name.Remove(name.IndexOf('_'));
        return int.Parse(name);
    }

    /// <summary>
    /// Get the number of icosidodecahedron's game object.
    /// </summary>
    /// <returns>Integer number of this icosidodecahedron's game object</returns>
    public int GetNumberIcosi() { return _icosiNumber; }

    /// <summary>
    /// Get the icosidodecahedron father number.
    /// </summary>
    /// <returns>Integer number of this icosidodecahedron's game object father</returns>
    public int GetIcosiFather() { return _icosiFather;}

    /// <summary>
    /// Set the icosidodecahedron father number.
    /// </summary>
    /// <param name="icosiFatherNumb">Integer number of icosidodecahedron father</param>
    public void SetFatherNumber(int icosiFatherNumb) { _icosiFather = icosiFatherNumb; }

    /// <summary>
    /// Set the face number.
    /// </summary>
    /// <param name="faceNumb">Integer number of face</param>
    public void SetFaceNumber(int faceNumb) { _faceNumber = faceNumb; }

    /// <summary>
    /// Set the numbere of the icosidodecahedron.
    /// </summary>
    /// <param name="number">Integer value of icosidodecahedron</param>
    public void SetNumberIcosi(int number) { _icosiNumber = number; }

    /// <summary>
    /// Activate the response to collsions
    /// </summary>
    public void Check() { _wait = false; }
}
