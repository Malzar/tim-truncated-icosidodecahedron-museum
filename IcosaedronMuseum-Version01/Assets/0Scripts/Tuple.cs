﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class to storage faces and corridors.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class Tuple {
    private int _face;
    private int _corridor1;
    private int _corridor2;

    /// <summary>
    /// Builder with one corridor
    /// </summary>
    /// <param name="face">Number of the face</param>
    /// <param name="corridor">Number of the corridor</param>
    public Tuple(int face, int corridor) {
        _face = face;
        _corridor1 = corridor;
        _corridor2 = -1;
    }

    /// <summary>
    /// Builder with two corridors.
    /// </summary>
    /// <param name="face">Number of the face.</param>
    /// <param name="corridorA">Number of the corridor 1.</param>
    /// <param name="corridorB">Number of the corridor 2.</param>
    public Tuple(int face, int corridorA, int corridorB) {
        _face = face;
        _corridor1 = corridorA;
        _corridor2 = corridorB;
    }
    /// <summary>
    /// Get the face.
    /// </summary>
    /// <returns>Return the integer number of the face.</returns>
    public int GetFirst() { return _face; }
    /// <summary>
    /// Get the first corridor.
    /// </summary>
    /// <returns>Return the number of the first corridor.</returns>
    public int GetSecond() { return _corridor1; }
    /// <summary>
    /// Get the second corridor.
    /// </summary>
    /// <returns>Return the number of the second corridor.</returns>
    public int GetThird() { return _corridor2; }
    /// <summary>
    /// Add the second corridor.
    /// </summary>
    /// <param name="corridor">Number of the second corridor.</param>
    public void Add(int corridor) { _corridor2 = corridor; }
    /// <summary>
    /// Operator equal.
    /// </summary>
    /// <param name="face">Number of the face to check.</param>
    /// <returns>Return true if the faces are same, false in other case.</returns>
    public bool Equal(int face) { return face == _face; }
}
