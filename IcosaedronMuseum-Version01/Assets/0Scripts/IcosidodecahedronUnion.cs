﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class storage the union data between icosidodecahedrons.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class IcosidodecahedronUnion{
	
	private Vector3 _newCenter;
	private int _limitFace;
	private int _borderFace;
	private int _icosiInceptionMode;
	
	public IcosidodecahedronUnion(){}
	/// <summary>
	/// Get the center of new icosidodecahedron
	/// </summary>
	/// <returns>Returna  Vector 3 with center position</returns>
	public Vector3 GetCenter(){ return _newCenter;}

    /// <summary>
    /// Get the limit face of icosidodecahedron father.
    /// </summary>
    /// <returns>The integer number of limit face</returns>
	public int GetLimitFace(){ return _limitFace;}

    /// <summary>
    /// Get the border face of icosidodecahedron father.
    /// </summary>
    /// <returns>The integer number of border face</returns> 
	public int GetBorderFace(){ return _borderFace;}

    /// <summary>
    /// Get the platform mode of icosidodecahedron father.
    /// </summary>
    /// <returns>The integuer number of platform mode</returns>
	public int GetInceptionMode(){ return _icosiInceptionMode;}

	/// <summary>
    /// Set the center of new icosidodecahedron
	/// </summary>
	/// <param name="center">The centre of new icosidodecahedron</param>
	public void SetCenter(Vector3 center){ _newCenter=center;}

    /// <summary>
    /// Set the limit face of icosidodecahedron father.
    /// </summary>
    /// <param name="limitFace">The number of limit face.</param>
	public void SetLimitFace(int limitFace){ _limitFace=limitFace;}

    /// <summary>
    /// Set the border face of icosidodecahedron father.
    /// </summary>
    /// <param name="borderFace">The number of border face.</param>
	public void SetBorderFace(int borderFace){ _borderFace=borderFace;}

    /// <summary>
    /// Set the platform mode of icosidodecahedron father.
    /// </summary>
    /// <param name="inceptionMode">The number of platform mode.</param>
	public void SetInceptionMode(int inceptionMode){ _icosiInceptionMode=inceptionMode;}

}
 