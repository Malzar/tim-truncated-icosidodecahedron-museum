﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class to save collision data of an icosidodecahedron.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class TupleList : System.IEquatable<TupleList> {
    private List<Tuple> _faceList;
    private int _icosiNumber;

    /// <summary>
    /// Builder.
    /// </summary>
    /// <param name="icosiNumber">The number of an icosidodecahedron.</param>
    public TupleList(int icosiNumber) {
        _icosiNumber = icosiNumber;
        _faceList = new List<Tuple>();
    }

    /// <summary>
    /// Add a face collided.
    /// </summary>
    /// <param name="face">Number of collided face.</param>
    /// <param name="corridor">Number of the corridor.</param>
    public void Add(int face, int corridor) {
        bool find = false;
        foreach (Tuple elem in _faceList) {
            if (elem.Equal(face)) {
                elem.Add(corridor);
                find = true;
                break;
            }
        }
        if (!find) 
            _faceList.Add(new Tuple(face, corridor));
    }

    /// <summary>
    /// Add a face collided.
    /// </summary>
    /// <param name="face">Number of collided face.</param>
    /// <param name="corridorA">Number of the first corridor.</param>
    /// <param name="corridorB">Number of the second corridor.</param>
    public void Add(int face, int corridorA, int corridorB) {
        _faceList.Add(new Tuple(face, corridorA, corridorB));
    }

    /// <summary>
    /// Get the number of the icosidodecahedron collided.
    /// </summary>
    /// <returns>Return the integer number.</returns>
    public int GetIcosiNumber() { return _icosiNumber; }

    /// <summary>
    /// Get all faces collided.
    /// </summary>
    /// <returns>Return a list with the number of faces collided.</returns>
    public List<Tuple> GetFaces() { return _faceList; }

    /// <summary>
    /// Operator equals.
    /// </summary>
    /// <param name="other">Tuple list to comparate.</param>
    /// <returns>Return true if the icosidodecahedron of the tuple list is the same, false in other case.</returns>
    public bool Equals(TupleList other) { return _icosiNumber == other.GetIcosiNumber(); }
}
