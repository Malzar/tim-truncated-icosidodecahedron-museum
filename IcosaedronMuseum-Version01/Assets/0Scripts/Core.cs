﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

/// <summary>
/// This is the main class of project.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class Core : MonoBehaviour {

    private RoadBuilder _builder;
    private List<int> _numFaceList;
    private List<int> _modePositionFaceList;
    private List<bool> _putAllFaceList;
    private GameObject _pj;
    private Vector3 _center;
    private Vector3 _oldPos;

    private int _cubeSize;
    private int _numFace;
    private int _modePositionFace;
    private int _lastDataFace;
    private int _seed;

    private bool _putAllFaces;
    private bool _putPj;
    private bool _changeViwer;
    private bool _isPjViwer;
    private bool _build;
    private bool _move; 
    private bool _wait;
    private bool _genSon;
    private bool _check;
    private bool _rebuild;
    private bool _stopViwer;
    private bool _hideTeleportPanel;
    private bool _hideMenu;

    //////////////////////////////////////
    //private GameObject box;
    //private GameObject controlBox;
    //private Stopwatch swCheck, swGenChild, swRebuild, swRepair, swTotal, swMove;
    //private long TIMECHEK, TIMEGEN, TIMEREPA, TIMEREBU;
    //System.IO.StreamWriter FCHEK,FGENCHILD,FREBU,FREP,FTOTAL, FDATASCENE;
    //private bool STOPTIMER, STOPTIMERCHECK;
    //private int TIINSCENE, FACESINSCENE, TIGENERATED, FACESGENERATED;
    //////////////////////////////////////

    /// <summary>
    /// Unity function. Initialize parameters with default values and reserve memory
    /// </summary>
    void Awake(){
        _numFaceList = new List<int>();
        _modePositionFaceList = new List<int>();
        _putAllFaceList = new List<bool>();

        _build = true;
        _genSon = true;
        _wait = false;
        _move = false;
        _check = false;
        _rebuild = false;
        _putPj = true;
        _changeViwer = false;
        _isPjViwer = true;
        _stopViwer = false;
        _hideTeleportPanel = true;
        _hideMenu = true;

        //////////////////////////////////////
        //swCheck = new Stopwatch();
        //swGenChild = new Stopwatch();
        //swRebuild = new Stopwatch();
        //swRepair = new Stopwatch();
        //swTotal = new Stopwatch();
        //swMove = new Stopwatch(); 
        //FCHEK= new System.IO.StreamWriter("CheckLoop.txt");
        //FGENCHILD = new System.IO.StreamWriter("GenChildLoop.txt");
        //FREBU = new System.IO.StreamWriter("RebuildLoop.txt");
        //FREP = new System.IO.StreamWriter("RepairLoop.txt");
        //FTOTAL = new System.IO.StreamWriter("PrincipalLoop.txt");
        //FDATASCENE = new System.IO.StreamWriter("DataScene.txt");
        //TIMECHEK = TIMEGEN = TIMEREBU = TIMEREPA = 0;
        //STOPTIMER = true;
        //STOPTIMERCHECK = false;
        //TIINSCENE = FACESINSCENE = TIGENERATED = FACESGENERATED = 0;
        //////////////////////////////////////
    }

    /// <summary>
    /// Unity function. Initialize parameters
    /// </summary>
	void Start () {
        SetData();
        GetDataIcosi();
        _builder = new RoadBuilder((float)_cubeSize / 2, _center, _seed);
        _builder.Start(1, _numFace, _modePositionFace, _putAllFaces);

        transform.FindChild("TeleportPanel").transform.FindChild("Panel").gameObject.SetActive(false);

        //box = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //box.transform.position = _center;
        //box.renderer.material.shader = Shader.Find("Transparent/Diffuse");
        //box.renderer.material.color = new Vector4(0f, 1f, 0f, 0.2f);
        //box.transform.localScale = new Vector3(_cubeSize, _cubeSize, _cubeSize);
        //box.GetComponent<BoxCollider>().enabled = false;

        //controlBox = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //controlBox.transform.position = _center;
        //controlBox.renderer.material.shader = Shader.Find("Transparent/Diffuse");
        //controlBox.renderer.material.color = new Vector4(1f, 0f, 0f, 0.2f);
        //controlBox.transform.localScale = new Vector3(_cubeSize - 50, _cubeSize - 50, _cubeSize - 50);
        //controlBox.GetComponent<BoxCollider>().enabled = false;
        //////////////
        //ReStartTimer(5);
        /////////////
	}

    /// <summary>
    /// Unity function. Main loop.
    /// </summary>
    void Update() {
        if (_hideMenu) {
            if (_build) {
                ////////////////
                //if (!STOPTIMER)
                    //ReStartTimer(5);
                ////////////////
                if (!_wait) {
                    //////////////////
                    //if(STOPTIMERCHECK)
                        //StopTimer(1);
                    //////////////////
                    if (_rebuild) {
                        //////////////
                        //ReStartTimer(3);
                        /////////////
                        _builder.Rebuild();
                        //////////////
                        //StopTimer(3);
                        /////////////
                        if (_builder.MoreToRebuild()) {
                            _check = true;
                            _wait = true;
                            ////////////// 
                            //ReStartTimer(1);
                            //STOPTIMERCHECK = false;
                            /////////////
                        } else {
                            //////////////
                            //ReStartTimer(4);
                            /////////////
                            _builder.Repair();
                            //////////////
                            //StopTimer(4);
                            /////////////
                            _wait = true;
                            _genSon = true;
                        }
                        _rebuild = false;
                    }
                    if (_check) {
                        _builder.Check();
                        _rebuild = true;
                        _check = false;
                        _wait = true;
                        ////////////
                        //STOPTIMERCHECK = true;
                        ////////////
                    }
                    if (_genSon) {
                        GetDataIcosi();
                        //////////////
                        //ReStartTimer(2);
                        /////////////
                        _build = _builder.GenerateSons(_numFace, _modePositionFace, _putAllFaces);
                        //////////////
                        //StopTimer(2);
                        /////////////
                        if (!_build) {
                            this.transform.FindChild("TeleportPanel").transform.FindChild("Panel").transform.FindChild("Scrollable").gameObject.GetComponent<TeleportPanel>().AddIcosiToTeleport(_builder.GetIcosiInScene());
                            this.transform.FindChild("TeleportPanel").FindChild("LoadingText").gameObject.SetActive(false);
                        } else {
                            _wait = true;
                            _check = true;
                            _genSon = false;
                            //////////////
                            //ReStartTimer(1);
                            //STOPTIMERCHECK = false;
                            /////////////
                        }
                    }
                } else {
                    _wait = false;
                }
            } else {
                //////////////
                //if(STOPTIMER)
                //    StopTimer(5);
                /////////////
                if (!_stopViwer) {
                    if (Input.GetKeyDown(KeyCode.C))
                        _changeViwer = true;
                    if (_putPj) {
                        _pj = _builder.PutPJ();
                        _putPj = false;
                    }
                    if (_changeViwer) {
                        ChangeViwer();
                    }
                    CheckPosition();
                    if (Input.GetKeyDown(KeyCode.T)) {
                        if (_hideTeleportPanel) {
                            StopWalk(false);
                        } else {
                            ContinueWalk();
                        }
                        this.transform.FindChild("TeleportPanel").transform.FindChild("Panel").gameObject.SetActive(_hideTeleportPanel);
                        _hideTeleportPanel = !_hideTeleportPanel;
                    }

                } else {
                    ContinueWalk();
                }
            }
            if (_move) {
                StopWalk(true);
                this.transform.FindChild("TeleportPanel").transform.FindChild("LoadingText").gameObject.SetActive(true);
                if (_isPjViwer) {
                    _pj.transform.position += _pj.transform.up * 0.01f;
                }
                ////////////////
                //ReStartTimer(6);
                ////////////////
                _builder.MoveSpaceCenter(_center);
                ////////////////
                //StopTimer(6);
                ////////////////
                _move = false;
                _build = true;
                _wait = true;
                //box.transform.position = _center;
                //controlBox.transform.position = _center;
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (_hideMenu) {
                transform.FindChild("TeleportPanel").transform.FindChild("MenuPanel").gameObject.SetActive(_hideMenu);
                _hideMenu = !_hideMenu;
                StopWalk(false);
            } else {
                ReturnFromMenu();
            }
        }
        //if (Input.GetKeyDown(KeyCode.F)) {
        //    FCHEK.Close();
        //    FGENCHILD.Close();
        //    FREBU.Close();
        //    FREP.Close();
        //    FTOTAL.Close();
        //    FDATASCENE.Close();
        //}
    }

    /// <summary>
    /// Hide menu panel
    /// </summary>
    public void ReturnFromMenu() {
        transform.FindChild("TeleportPanel").transform.FindChild("MenuPanel").gameObject.SetActive(false);
        _hideMenu = !_hideMenu;
        ContinueWalk();
    }

    /// <summary>
    /// Get the pattern parameters from initial scene.
    /// </summary>
    public void SetData(){
        Data dataSaved= GameObject.Find("Data").GetComponent<Data>();

        foreach (int elem in dataSaved.GetNumberOfFaces())
            _numFaceList.Add(elem);

        foreach (int elem in dataSaved.GetFaceMode())
            _modePositionFaceList.Add(elem);

        foreach (bool elem in dataSaved.GetPutAllFaces())
            _putAllFaceList.Add(elem);

        _cubeSize = dataSaved.GetSizeView();
        _center = new Vector3(0,0,0);
        _oldPos = _center; 
        _seed = dataSaved.GetSeed();
    }

    /// <summary>
    /// Add a colision
    /// </summary>
    /// <param name="icosiNumber"> Number of the collided icosidodecahedron</param>
    /// <param name="faceNumber"> Number of the collided face </param>
    public void AddColision(int icosiNumber,int faceNumber){
        _builder.AddColision(icosiNumber, faceNumber);
    }

    /// <summary>
    /// Teleport to random face from an icosidodecahedron
    /// </summary>
    /// <param name="icosiNumber">Number of the icosidodecahedron to teleport</param>
    public void Teleport(int icosiNumber) {
        transform.FindChild("TeleportPanel").transform.FindChild("Panel").gameObject.SetActive(false);
        _hideTeleportPanel = true;
        if (!_build) {
            Destroy(_pj);
            _pj = _builder.PutPJ(icosiNumber);
        }
        _isPjViwer = true;
    }

    /// <summary>
    /// Set values from next pattern.
    /// </summary>
    private void GetDataIcosi(){
        if(_lastDataFace==_modePositionFaceList.Count)
            _lastDataFace = 0;

        _modePositionFace = _modePositionFaceList[_lastDataFace];
        _numFace = _numFaceList[_lastDataFace];
        _putAllFaces = _putAllFaceList[_lastDataFace];

        _lastDataFace++;
    }

    /// <summary>
    /// Evalueate if player is inside the limit of icosidodecahedrons representation.
    /// </summary>
    private void CheckPosition(){
        float tamLimit = (float)_cubeSize/6;
        tamLimit *= 1.2f;
        Vector3 increment = new Vector3(0f, 0f, 0f);

        if ((_oldPos - _pj.transform.position).sqrMagnitude > 2f) {

            if (_pj.transform.position.x > (_center.x + tamLimit))
                increment += new Vector3((float)_cubeSize / 3, 0f, 0f);
            if (_pj.transform.position.x < (_center.x - tamLimit))
                increment -= new Vector3((float)_cubeSize / 3, 0f, 0f);

            if (_pj.transform.position.y > (_center.y + tamLimit))
                increment += new Vector3(0f, (float)_cubeSize / 3, 0f);
            if (_pj.transform.position.y < (_center.y - tamLimit))
                increment -= new Vector3(0f, (float)_cubeSize / 3, 0f);

            if (_pj.transform.position.z > (_center.z + tamLimit))
                increment += new Vector3(0f, 0f, (float)_cubeSize / 3);
            if (_pj.transform.position.z < (_center.z - tamLimit))
                increment -= new Vector3(0f, 0f, (float)_cubeSize / 3);
        }

        if (increment != new Vector3(0f, 0f, 0f)){
            _center += increment;
            _move = true;
            _oldPos = _pj.transform.position;
        }
    }

    /// <summary>
    /// Change between free camera and walk-through
    /// </summary>
    private void ChangeViwer() {
        _changeViwer = false;
        Vector3 position = _pj.transform.position;

        if (!_isPjViwer) {
            RaycastHit platformHited;
            float distance = 51;
            string name="";
            for (int y = -1; y < 2; y++){
                for (int z = -1; z < 2; z++){
                    for (int x = -1; x < 2; x++){
                        Vector3 direction = new Vector3(x,y,z);
                        if (Physics.Raycast(_pj.transform.position, direction, out platformHited, 50f)) {
                            if (platformHited.transform.parent.tag != "Platform") {
                                if (platformHited.distance < distance) {
                                    name = platformHited.transform.parent.name;
                                    distance = platformHited.distance;
                                }
                            }
                        }
                    }
                }
            }
            Destroy(_pj);
            if (name != "") {
                name = name.Substring(name.IndexOf('_') + 1);
                int aux;
                if (int.TryParse(name, out aux)) 
                    _pj = _builder.PutPJ(int.Parse(name));                
            } else {
                _pj = _builder.PutPJ(-1);
                if (_pj== null){
                    _isPjViwer = !_isPjViwer;
                    _pj = GameObject.Instantiate(Resources.Load("FreeMoveCamera")) as GameObject;
                    _pj.transform.position = position;
                } 
            }
        } else {
            Destroy(_pj);
            _pj = GameObject.Instantiate(Resources.Load("FreeMoveCamera")) as GameObject;
            _pj.transform.position = position;
        }
        _isPjViwer = !_isPjViwer;
    }

    /// <summary>
    /// Freeze player movement.
    /// </summary>
    /// <param name="stopViwer">True if is to load, false if is to unhide panel </param>
    private void StopWalk(bool stopViwer) {
        _stopViwer = stopViwer;
        if (_isPjViwer) {
            _pj.GetComponent<FirstPersonController>().Stop();
            _pj.transform.FindChild("ColliderControler").GetComponent<ColliderController>().Stop();
        } else {
            _pj.GetComponent<CameraFreeMove>().Stop();
            _pj.GetComponent<CameraOrbit>().Stop();
        }
    }

    /// <summary>
    /// Unfreeze player movement.
    /// </summary>
    private void ContinueWalk() {
        if (_isPjViwer) {
            _pj.transform.FindChild("ColliderControler").GetComponent<ColliderController>().Play();
            _pj.GetComponent<FirstPersonController>().Play();
        } else {
            _pj.GetComponent<CameraFreeMove>().Play();
            _pj.GetComponent<CameraOrbit>().Play();
        }
        _stopViwer = false;
    }

//    public void ReStartTimer(int timer) {
//        switch (timer) {
//            case 1:
//                swCheck.Reset();
//                swCheck.Start();
//                break;
//            case 2:
//                swGenChild.Reset();
//                swGenChild.Start();
//                break;
//            case 3:
//                swRebuild.Reset();
//                swRebuild.Start();
//                break;
//            case 4:
//                swRepair.Reset();
//                swRepair.Start();
//                break;
//            case 5:
//                swTotal.Reset();
//                swTotal.Start();
//                STOPTIMER = true;
//                break;
//            case 6:
//                swMove.Reset();
//                swMove.Start();
//                break;
//            default:
//                break;
//        }
//    }

//    public void StopTimer(int timer) {
//        switch (timer) {
//            case 1:
//                swCheck.Stop();
//                FCHEK.WriteLine("TI to check: " + _builder.TICHECK.ToString() + "-Ti bigger than 1 face: " + _builder.TICHEMOREFACE.ToString());
//                FCHEK.WriteLine("Time to ckeck: " + swCheck.ElapsedMilliseconds.ToString());
//                FTOTAL.WriteLine("TI to check: " + _builder.TICHECK.ToString());
//                FTOTAL.WriteLine("Time to ckeck: " + swCheck.ElapsedMilliseconds.ToString() + "ms");
//                TIMECHEK+=swCheck.ElapsedMilliseconds;
//                break;
//            case 2:
//                swGenChild.Stop();
//                FGENCHILD.WriteLine("TI used: "+_builder.TIFATHER.ToString()+ "-TI generated: "+_builder.TICHILD.ToString()+"-faces created: "+(_numFace*_builder.TICHILD).ToString());
//                FGENCHILD.WriteLine("Time used to generate child: " + swGenChild.ElapsedMilliseconds.ToString());
//                FTOTAL.WriteLine("TI used to gen: "+_builder.TIFATHER.ToString()+ " TI generated: "+_builder.TICHILD.ToString()+" faces created: "+(_numFace*_builder.TICHILD).ToString());
//                FTOTAL.WriteLine("Time used to generate child: " + swGenChild.ElapsedMilliseconds.ToString() + "ms");
//                TIMEGEN+=swGenChild.ElapsedMilliseconds;
//                TIGENERATED += _builder.TICHILD;
//                FACESGENERATED += _builder.TICHILD * _numFace;
//                break;
//            case 3:
//                swRebuild.Stop();
//                FREBU.WriteLine("TI to rebuild: "+_builder.TIREBUILD.ToString()+"-TI bigger than 1 face: "+ _builder.TIWITHMOREFACES.ToString());
//                FREBU.WriteLine("Time to rebuild: " + swRebuild.ElapsedMilliseconds.ToString());
//                FTOTAL.WriteLine("TI to rebuild: " + _builder.TIREBUILD.ToString() + " TI bigger than 1 face: " + _builder.TIWITHMOREFACES.ToString());
//                FTOTAL.WriteLine("Time to rebuild: " + swRebuild.ElapsedMilliseconds.ToString() + "ms");
//                TIMEREBU += swRebuild.ElapsedMilliseconds;
//                break;
//            case 4:
//                swRepair.Stop();
//                FREP.WriteLine("TI to repair: " + _builder.TIREPAIR.ToString() + "-faces repaired: " + _builder.TIFACESREP.ToString());
//                FREP.WriteLine("Time to repair: " + swRepair.ElapsedMilliseconds.ToString());
//                FTOTAL.WriteLine("TI to repair: " + _builder.TIREPAIR.ToString() + " faces repaired: " + _builder.TIFACESREP.ToString());
//                FTOTAL.WriteLine("Time to repair: " + swRepair.ElapsedMilliseconds.ToString() + "ms");
//                TIMEREPA += swRepair.ElapsedMilliseconds;
//                break;
//            case 5:
//                swTotal.Stop();
//                CountElementInScene();
//                //FCHEK.WriteLine("/////////////////////////////////////////////////////////////////////////////");
//                //FGENCHILD.WriteLine("/////////////////////////////////////////////////////////////////////////////");
//                //FREBU.WriteLine("/////////////////////////////////////////////////////////////////////////////");
//                //FREP.WriteLine("/////////////////////////////////////////////////////////////////////////////");
//                FTOTAL.WriteLine("/////////////////////////////////////////////////////////////////////////////");
//                FDATASCENE.WriteLine("Time total to gen scene: " + (swTotal.ElapsedMilliseconds + swMove.ElapsedMilliseconds).ToString());
//                FDATASCENE.WriteLine("children: " + TIMEGEN.ToString() + " rebuild: " + TIMEREBU.ToString() + " repair: " + TIMEREPA.ToString() + " check:" + TIMECHEK.ToString() + " move: " + swMove.ElapsedMilliseconds.ToString());
//                FDATASCENE.WriteLine("TI Saved: " + _builder.TISAVED.ToString() + " TI loaded: " + _builder.TILOADED.ToString());
//                FDATASCENE.WriteLine("TI generated: " + TIGENERATED.ToString() + " faces generated: " + FACESGENERATED.ToString());
//                FDATASCENE.WriteLine("TI in scene: " + TIINSCENE.ToString() + " faces in scene: " + FACESINSCENE.ToString());
//                FDATASCENE.WriteLine("*****************************************************************************");
//                STOPTIMER = false;
//                TIMECHEK = TIMEGEN = TIMEREBU = TIMEREPA = 0;
//                TIINSCENE = FACESINSCENE = TIGENERATED = FACESGENERATED = 0;
//                break;
//            case 6:
//                swMove.Stop();
//                break;
//            default:
//                break;
//        }
//    }

//    public void CountElementInScene() {
//        GameObject[] temp;
//        temp = GameObject.FindGameObjectsWithTag("Platform");
//        FACESINSCENE = temp.Length;
//        temp = GameObject.FindGameObjectsWithTag("TI");
//        TIINSCENE = temp.Length;
//    }
}
