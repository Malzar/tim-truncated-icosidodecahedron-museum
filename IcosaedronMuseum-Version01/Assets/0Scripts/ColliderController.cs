﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class responsible for controlling for the player collider
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class ColliderController : MonoBehaviour {

	private GameObject _actualPlatform;
    private GameObject _oldPlatform;
    private string _securityName;
    private bool _wait;
    private GameObject _teleportPanel;

    /// <summary>
    /// Unity function. Initialize parameters
    /// </summary>
    void Awake() {
        _wait = false;
        _actualPlatform = this.gameObject;
        _oldPlatform = _actualPlatform;
    }

    /// <summary>
    /// Stop to check collsions, deactivate gravitiy and convex the platform. 
    /// </summary>
    public void Stop() {
        _actualPlatform.GetComponent<GravityAttractor>().Stop();
        _actualPlatform.GetComponent<MeshCollider>().convex = true;
        _oldPlatform.GetComponent<MeshCollider>().convex = true;
        _securityName = _actualPlatform.name;
        _wait = true;
    }

    /// <summary>
    /// Unconvex the platform which is the player and activate gravity.
    /// If player's platform is modifies, add the necesary parameters.
    /// </summary>
    public void Play() {
        if (_actualPlatform == null) {
            _actualPlatform = GameObject.Find(_securityName);
            _actualPlatform.GetComponent<MeshCollider>().convex = false;
            _actualPlatform.AddComponent<GravityAttractor>();
            transform.parent.GetComponent<GravityBody>().SetPlatform(_actualPlatform);
        } else {
            _actualPlatform.GetComponent<MeshCollider>().convex = false;
            _actualPlatform.GetComponent<GravityAttractor>().Play();
        }
        _wait = false;
    }

    /// <summary>
    /// Add a GameObject platform as actual platform. Add all platform needed component.
    /// </summary>
    /// <param name="actualPlatform"> GameObject platform to add as actual platform</param>
    public void SetInitialPlatformPlatform(GameObject actualPlatform) {
        _teleportPanel = GameObject.Find("Core").transform.FindChild("TeleportPanel").FindChild("Panel").FindChild("Scrollable").gameObject;
        _actualPlatform = actualPlatform;
        _oldPlatform = _actualPlatform;
        _actualPlatform.GetComponent<MeshCollider>().convex = false;
        
        _teleportPanel.GetComponent<TeleportPanel>().SetActualIcosi(_actualPlatform.name);
    }

    /// <summary>
    /// Check the collision with other game objects.
    /// </summary>
    /// <param name="other"> Game object collided</param>
    void OnTriggerEnter(Collider other) {
        if (!_wait) {
            if (other.tag == "Platform") {
                if (other.name != _actualPlatform.name) {
                    if (_oldPlatform.GetComponent<MeshCollider>() != null)
                        _oldPlatform.GetComponent<MeshCollider>().convex = true;
                    if (_actualPlatform.GetComponent<GravityAttractor>() != null)
                        Destroy(_actualPlatform.GetComponent<GravityAttractor>());

                    _oldPlatform = _actualPlatform;
                    _actualPlatform = other.gameObject;
                    _actualPlatform.GetComponent<MeshCollider>().convex = false;
                    _actualPlatform.AddComponent<GravityAttractor>();
                    transform.parent.GetComponent<GravityBody>().SetPlatform(_actualPlatform);
                    
                    _teleportPanel.GetComponent<TeleportPanel>().SetActualIcosi(_actualPlatform.name);
                }
            }
        }
    }
}
