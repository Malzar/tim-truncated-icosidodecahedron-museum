﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class storage a list of icosidodecahedrons.
/// </summary>
/// <author email="franciscoMurea@gmail.com"> Francisco M Urea <see href="http://franciscomurea.es/"/></author>
/// <date>20/4/2015</date>
public class IcosidodecahedronList {

    private List<Icosidodecahedron> _icosidodecahedrons;

    /// <summary>
    /// Builder.
    /// </summary>
    public IcosidodecahedronList() {
        _icosidodecahedrons = new List<Icosidodecahedron>();
    }

    /// <summary>
    /// Destroy all icosiodecahedron data from the list.
    /// </summary>
    public void Clear() {
        while (!IsEmpty())
            GetNext().Destroy();
    }
    /// <summary>
    ///Get all icosidodecahedron.
    /// </summary>
    /// <returns>Return a list with all icosidodecahedron.</returns>
    public List<Icosidodecahedron> GetList() { return _icosidodecahedrons; }
    
    /// <summary>
    /// Add an icosiododechaedron to the list.
    /// </summary>
    /// <param name="Icosi">Icosiodecaheron element to add.</param>
    public void Add(Icosidodecahedron Icosi) { _icosidodecahedrons.Add(Icosi); }

    /// <summary>
    /// Add several icosidodecahedron into the list.
    /// </summary>
    /// <param name="icosiList">Icosidodecahedron list with the icosiodechaedron to add.</param>
    public void AddRange(IcosidodecahedronList icosiList) { _icosidodecahedrons.AddRange(icosiList.GetList()); }

    /// <summary>
    /// Check if the list is empty.
    /// </summary>
    /// <returns>Return true if list is empty, false in other case.</returns>
    public bool IsEmpty() {
        if (_icosidodecahedrons.Count != 0) 
            return false;
        else 
            return true;
    }

    /// <summary>
    /// Get the first icosidodecahedron and erase from the list.
    /// </summary>
    /// <returns>Return the first Icosidodecahedron element.</returns>
    public Icosidodecahedron GetNext() {
        if (_icosidodecahedrons.Count != 0) {
            Icosidodecahedron icosi = null;
            icosi = _icosidodecahedrons[0];
            //_icosidodecahedrons[0].Destroy();
            _icosidodecahedrons.RemoveAt(0);
            return icosi;
        } else {
            _icosidodecahedrons = null;
            return null;
        }
    }

    /// <summary>
    /// Get the icosidodechaedron from a concrete position and erase it.
    /// </summary>
    /// <param name="IcosiNumber">Position of the icosidodecahedron in the list.</param>
    /// <returns>Retrun a icosidodecahedron element if exist or null if the position not exist.</returns>
    public Icosidodecahedron GetIcosi(int IcosiNumber) {
        Icosidodecahedron icosi = null;
        int pos = 0;

        foreach (Icosidodecahedron elem in _icosidodecahedrons) {
            if (elem.GetNumber() == IcosiNumber) {
                icosi = elem;
                break;
            }
            pos++;
        }
        if (pos < _icosidodecahedrons.Count)
            _icosidodecahedrons.RemoveAt(pos);
        return icosi;
    }

    /// <summary>
    /// Get the number of element in the list.
    /// </summary>
    /// <returns>The integer number of element in the list.</returns>
    public int Count() { return _icosidodecahedrons.Count; }

    /// <summary>
    /// Get a reference to icosidodecahedron element from especific position in the list.
    /// </summary>
    /// <param name="Pos">Position number of the list.</param>
    /// <returns>Return a reference of a icosidodecahedron in the list or null if position not exist.</returns>
    public Icosidodecahedron GetByPos(int Pos) {
        if (Pos < 0 || Pos > _icosidodecahedrons.Count)
            return null;
        else
            return _icosidodecahedrons[Pos];
    }

    /// <summary>
    /// Get a reference to especific icosidodecahedron element in the list.
    /// </summary>
    /// <param name="IcosiNumber">Number off the icosidodecahedron.</param>
    /// <returns>Return a reference of a icosidodecahedron in the list or null if icosidodecahedron not exist.</returns>
    public Icosidodecahedron GetByNumber(int IcosiNumber) {
        Icosidodecahedron icosi = null;

        foreach (Icosidodecahedron elem in _icosidodecahedrons) {
            if (elem.GetNumber() == IcosiNumber) {
                icosi = elem;
                break;
            }
        }
        return icosi;
    }

    /// <summary>
    /// Remove a specific icosidodecahedron data.
    /// </summary>
    /// <param name="Pos">Position number of the list.</param>
    public void DeleteAt(int Pos) {
        //Destroy and delete from list
        _icosidodecahedrons[Pos].Destroy();
        _icosidodecahedrons.RemoveAt(Pos);
    }
}
